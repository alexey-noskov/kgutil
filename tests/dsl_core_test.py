import pytest

from kgutil import dsl


class StubNode(dsl.Node):
    pass


def test_add_node():
    with dsl.Graph() as g:
        g.add(StubNode('aaa'))

        assert len(g.nodes) == 1
        assert g.nodes['aaa'].name == 'aaa'
        assert g.aaa.name == 'aaa'


def test_nameless_node_fails():
    with dsl.Graph() as g:
        with pytest.raises(ValueError):
            g.add(StubNode())

        assert len(g.nodes) == 0


def test_duplicate_node_fails():
    with dsl.Graph() as g:
        g.add(StubNode('aaa'))
        with pytest.raises(ValueError):
            g.add(StubNode('aaa'))

        assert len(g.nodes) == 1


def test_assign_node():
    with dsl.Graph() as g:
        g.bbb = StubNode()

        assert len(g.nodes) == 1
        assert g.nodes['bbb'].name == 'bbb'
        assert g.bbb.name == 'bbb'


def test_add_node_with_name():
    with dsl.Graph() as g:
        g.add(StubNode().with_name('bbb'))

        assert len(g.nodes) == 1
        assert g.nodes['bbb'].name == 'bbb'
        assert g.bbb.name == 'bbb'
