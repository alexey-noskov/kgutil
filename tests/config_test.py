from kgutil import config


def test_item_access():
    config.update(aaa={'bbb': 'x', 'ccc': 5})

    assert config['aaa']['bbb'] == 'x'
    assert config['aaa']['ccc'] == 5


def test_attr_access():
    config.update(aaa={'bbb': 'x', 'ccc': 5})

    assert config.aaa.bbb == 'x'
    assert config.aaa.ccc == 5


def test_scoped_override():
    config.update(aaa={'bbb': 'x', 'ccc': 5})

    with config.scoped(aaa=dict(bbb='f')):
        assert config.aaa.bbb == 'f'
        assert config.aaa.ccc == 5

    assert config.aaa.bbb == 'x'


def test_section_copy():
    config.update(aaa={'bbb': 'x', 'ccc': 'j'})

    aaa = config.aaa

    config.update(aaa={'bbb': 'y'})

    assert config.aaa.bbb == 'y'
    assert config.aaa.ccc == 'j'

    assert aaa.bbb == 'x'


def test_full_copy():
    config.update(aaa={'bbb': 'x', 'ccc': 5})

    copy = config.copy()

    config.update(aaa={'bbb': 'y'})

    assert config.aaa.bbb == 'y'
    assert config.aaa.ccc == 5

    assert copy.aaa.bbb == 'x'
