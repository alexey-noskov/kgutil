from kgutil import dsl, execution

from kgutil.testing.matching import matches, Any


def test_data_execute():
    with dsl.Graph() as g:
        g.data = dsl.Data(None)

        new_state, out = g.data.read(None, 'train')

        assert new_state is None
        assert isinstance(out, execution.Artifact)


def test_transformer_execute():
    with dsl.Graph() as g:
        g.data = dsl.Data(None)
        g.tr = dsl.Transformer(lambda x: x, [g.data])

        s1, o1 = g.tr.fit_transform(None, 'train')

        assert isinstance(o1, execution.Artifact)
        assert matches(o1.name, ['tr', 'fit_transform', Any(), 'result'])

        s2, o2 = g.tr.transform(s1, 'val')

        assert isinstance(o2, execution.Artifact)
        assert s1 == s2

        assert matches(o2.name, ['tr', 'transform', Any(), 'result'])

        s3, o3 = g.tr.transform(s1, 'test')

        assert isinstance(o2, execution.Artifact)
        assert s1 == s3

        assert matches(o3.name, ['tr', 'transform', Any(), 'result'])
        assert o2.name != o3.name
