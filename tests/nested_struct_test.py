from kgutil.util.nested_struct import map_values, filter_values, get_value


def test_map_values():
    assert map_values(lambda x: x + 1, [3, 4, {'a': 11}]) == [4, 5, {'a': 12}]


def test_map_values_multi():
    assert map_values(lambda x, y: x + y, [3, 4, {'a': 11}], [1, 2, {'a': 3}]) == [4, 6, {'a': 14}]


def test_filter_values():
    assert filter_values(lambda x: x % 2 == 0, [3, 4, {'a': 12, 'b': 13}]) == [4, {'a': 12}]


def test_get_value():
    assert get_value([3, 4, {'a': 12, 'b': 13}], [1]) == 4
    assert get_value([3, 4, {'a': 12, 'b': 13}], [2, 'a']) == 12
