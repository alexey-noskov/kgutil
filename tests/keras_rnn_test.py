import pandas as pd

import os
import tempfile

from sklearn.metrics import roc_auc_score
from kgutil.models.keras import KerasRNN


def test_fits_on_single_text_with_single_output():
    model = KerasRNN(num_epochs=1, batch_size=2, compile_opts=dict(loss='binary_crossentropy', optimizer='adam'), external_metrics=dict(auc=roc_auc_score))

    X = pd.DataFrame({'text': ['hi! my name is alex', 'what do you mean?', 'what is ypur name?']})
    y = pd.Series([1, 0, 1])

    model.fit(X, y)


def test_fits_on_multi_texts_and_numeric_with_multi_outputs():
    model = KerasRNN(num_epochs=1, batch_size=2, compile_opts=dict(loss='binary_crossentropy', optimizer='adam'), model_opts=dict(out_activation='sigmoid'))

    X = pd.DataFrame({'text1': ['hi! my name is alex', 'what do you mean?', 'what is ypur name?'], 'text2': ['one', 'two', 'three'], 'num1': [1.0, 2.0, 3.0]})
    y = pd.DataFrame({'a': [1, 0, 1], 'b': [2, 1, 0]})

    model.fit(X, y)

    with tempfile.TemporaryDirectory() as tmp_dir:
        model.save(os.path.join(tmp_dir, 'model'))
        model.load(os.path.join(tmp_dir, 'model'))
