from kgutil.execution import Artifact, Task


def test_artifact_name():
    a = Artifact(Task(['aaa']), 'bb')

    assert str(a.name) == 'aaa/bb'
