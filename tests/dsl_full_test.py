import numpy as np
import pytest

from kgutil import dsl, data, execution

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import FunctionTransformer, MinMaxScaler
from sklearn.exceptions import NotFittedError


class StupidModelTransformer(object):

    def __init__(self, model):
        self.model = model

    def fit_transform(self, X, y):
        self.model.fit(X, y)
        print(self.model.coef_)
        return self.model.predict(X)

    def transform(self, X):
        return self.model.predict(X)


def test_data_read():
    with dsl.Graph() as g:
        g.data = dsl.Data(data.ConstDataReader(train=[1, 2, 3]))

        with execution.Graph():
            api = dsl.StatefulApi(g.data, ['read'])
            assert api.read('train') == [1, 2, 3]


def test_stateless_transformer():
    with dsl.Graph() as g:
        g.data = dsl.Data(data.ConstDataReader(train=list(range(5))))
        g.f1 = dsl.Transformer(FunctionTransformer(lambda x: [v*2 for v in x], validate=False), [g.data])

        with execution.Graph():
            api = dsl.StatefulApi(g.f1, ['transform'])
            assert api.transform('train') == [0, 2, 4, 6, 8]


def test_statefull_transformer():
    with dsl.Graph() as g:
        g.data = dsl.Data(data.ConstDataReader(train=np.arange(5)[:, None].astype(np.float64), dev=np.arange(3, 6)[:, None].astype(np.float64)))
        g.feature = dsl.Transformer(MinMaxScaler(), [g.data])

        with execution.Graph():
            api = dsl.StatefulApi(g.feature, ['fit_transform', 'transform'])

            with pytest.raises(NotFittedError):
                api.transform('dev')

        with execution.Graph():
            api = dsl.StatefulApi(g.feature, ['fit_transform', 'transform'])

            assert np.array_equal(api.fit_transform('train'), [[0], [0.25], [0.5], [0.75], [1.0]])
            assert np.array_equal(api.transform('dev'), [[0.75], [1.0], [1.25]])


def test_lambda_transformer():
    with dsl.Graph() as g:
        g.data = dsl.Data(data.ConstDataReader(train=list(range(5))))
        g.f1 = dsl.Transformer(lambda x: [v+1 for v in x], [g.data])

        with execution.Graph():
            api = dsl.StatefulApi(g.f1, ['transform'])
            assert api.transform('train') == [1, 2, 3, 4, 5]


def test_multiinput_transformer():
    with dsl.Graph() as g:
        g.data = dsl.Data(data.ConstDataReader(train=list(range(5))))
        g.f1 = dsl.Transformer(lambda x: [v*v for v in x], [g.data])
        g.f2 = dsl.Transformer(lambda x, y: [a+b for a, b in zip(x, y)], [g.data, g.f1])

        with execution.Graph():
            api = dsl.StatefulApi(g.f2, ['transform'])
            assert api.transform('train') == [0, 2, 6, 12, 20]


def test_statefull_and_multiinput_transformer_chain():
    with dsl.Graph() as g:
        g.data = dsl.Data(data.ConstDataReader(train=np.arange(5)[:, None].astype(np.float64), dev=np.arange(3, 6)[:, None].astype(np.float64)))
        g.std = dsl.Transformer(MinMaxScaler(), [g.data])
        g.feature = dsl.Transformer(lambda x, y: x + y, [g.data, g.std])

        with execution.Graph():
            api = dsl.StatefulApi(g.feature, ['fit_transform', 'transform'])

            with pytest.raises(NotFittedError):
                api.transform('dev')

        with execution.Graph():
            api = dsl.StatefulApi(g.feature, ['fit_transform', 'transform'])

            assert np.array_equal(api.fit_transform('train'), [[0], [1.25], [2.5], [3.75], [5.0]])
            assert np.array_equal(api.transform('dev'), [[3.75], [5.0], [6.25]])


def test_statefull_transformer_fitted_on_other_part():
    with dsl.Graph() as g:
        g.data = dsl.Data(data.ConstDataReader(dev=np.arange(3, 6)[:, None].astype(np.float64), other=np.arange(5, 10)[:, None].astype(np.float64)))
        g.feature = dsl.Transformer(MinMaxScaler(), g.data).fit_on('other')

        with execution.Graph():
            api = dsl.StatefulApi(g.feature, ['fit_transform', 'transform'])

            assert np.array_equal(api.transform('dev'), [[-0.5], [-0.25], [0]])


def test_statefull_transformer_fitted_with_target():
    with dsl.Graph() as g:
        g.x = dsl.Data(data.ConstDataReader(train=np.array([[0], [1], [2], [2], [3]]), dev=np.arange(3, 6)[:, None].astype(np.float64)))
        g.y = dsl.Data(data.ConstDataReader(train=np.array([0, 2, 3.5, 4.5, 6])))

        g.p = dsl.Transformer(StupidModelTransformer(LinearRegression()), g.x).fit_with(g.y)

        with execution.Graph():
            api = dsl.StatefulApi(g.p, ['fit_transform', 'transform'])

            with pytest.raises(NotFittedError):
                api.transform('dev')

        with execution.Graph():
            api = dsl.StatefulApi(g.p, ['fit_transform', 'transform'])

            assert np.array_equal(api.fit_transform('train'), [0, 2, 4, 4, 6])
            assert np.array_equal(api.transform('dev'), [6, 8, 10])


def test_result_caching():
    c = [0]

    def transform(x):
        c[0] += 1
        return [v + c[0] for v in x]

    with dsl.Graph() as g:
        g.data = dsl.Data(data.ConstDataReader(train=list(range(3))))
        g.feature = dsl.Transformer(transform, [g.data])

        with execution.Graph():
            api = dsl.StatefulApi(g.feature, ['transform'])
            assert api.transform('train') == [1, 2, 3]
            assert api.transform('train') == [1, 2, 3]


# TODO Check cache consistency
# TODO Auxilary output usage
# TODO Caching of all task outputs

# TODO Caching in file system

# TODO Model interface
