= Kgutil

== Module description

=== Models

Model has following interface:

fit(X, y)
fit_eval(X, y) - optional

predict(X, y)

=== Encoders

=== Dsl

DataSource - reads some split of data, provides output schema
Transformer - data processing step, may be applied to data it was fitted on
StatelessTransformer - transformer which don't need to be fitted on given data and may be just applied
Model - predict things on new data, should not be applied to own data
Service - just generate some part of execution graph by calling methods

=== Execution

Execution graph specifies how to compute artifacts though execution of different tasks

Task - something what may be executed, receiving artifacts as inputs and producing new ones as outputs
Artifact - some immutable data which is produced by some tasks


ModelFit
ModelPredict

TransformerFitTransform
TransformerTransform


== Cache structure

Internal name is tuple

task_name = :node/:op/md5(options+inputs)

cache/:task_name
  meta.json
  :out_name.:ext
