from setuptools import setup, find_packages

setup(
    name='kgutil',
    version='0.1.0',
    description='Various utilities for Kaggle competitions',
    long_description='Various utilities for Kaggle competitions',
    url='https://github.com/alno/kgutil',
    author='Alexey Noskov',
    author_email='alexey.noskov@gmail.com',
    license='MIT',
    keywords='sample setuptools development',

    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    entry_points={
        'console_scripts': [
            'kgutil=kgutil:main',
        ],
    },

    install_requires=['scikit-learn', 'pandas', 'numpy', 'scipy', 'attrdict', 'six', 'lazy_import'],
    extras_require={
        'dev': ['check-manifest'],
        'test': ['pytest', 'coverage', 'Keras==2.1.3', 'tensorflow', 'h5py'],
    },
)
