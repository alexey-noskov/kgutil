import subprocess


def decompress_cmd(inp_file):
    inp_ext = inp_file.rsplit('.', 1)[-1]

    if inp_ext == 'zip':
        return "unzip -p {}".format(inp_file)
    elif inp_ext in ['gzip', 'gz']:
        return "gunzip -c {}".format(inp_file)
    else:
        raise RuntimeError("Unknown compression: %r" % inp_ext)


def compress_cmd(out_file):
    out_ext = out_file.rsplit('.', 1)[-1]

    if out_ext == 'zip':
        return "zip {}".format(out_file)
    elif out_ext in ['gzip', 'gz']:
        return "gzip > {}".format(out_file)
    else:
        raise RuntimeError("Unknown compression: %r" % out_ext)


def recompress_file(inp_file, out_file):
    subprocess.call("{} | {}".format(decompress_cmd(inp_file), compress_cmd(out_file)), shell=True)
