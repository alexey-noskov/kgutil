

def iter_values(s):
    if isinstance(s, (list, tuple)):
        for x in s:
            for v in iter_values(x):
                yield v
    elif isinstance(s, dict):
        for x in s.values():
            for v in iter_values(x):
                yield v
    else:
        yield s


def list_values(s):
    return list(iter_values(s))


def map_values(f, *s):
    assert len(s) > 0
    if isinstance(s[0], list):
        return list(map_values(f, *v) for v in zip(*s))
    elif isinstance(s[0], tuple):
        return tuple(map_values(f, *v) for v in zip(*s))
    elif isinstance(s[0], dict):
        return dict((k, map_values(f, *[d[k] for d in s])) for k in s[0].keys())
    else:
        return f(*s)


def filter_values(p, s):
    def accept(v):
        return isinstance(v, (list, tuple, dict)) or p(v)

    if isinstance(s, list):
        return list(filter_values(p, v) for v in s if accept(v))
    elif isinstance(s, tuple):
        return tuple(filter_values(p, v) for v in s if accept(v))
    elif isinstance(s, dict):
        return dict((k, filter_values(p, v)) for k, v in s.items() if accept(v))
    else:
        return s


def drop_empty_values(s, empty=[None]):
    return filter_values(lambda x: x not in empty, s)


def get_value(s, idx):
    if isinstance(s, (list, tuple, dict)):
        return get_value(s[idx[0]], idx[1:])
    else:
        assert len(idx) == 0
        return s
