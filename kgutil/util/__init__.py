import numpy as np
import scipy.sparse as sp

import six.moves.cPickle as pickle
from time import time
from contextlib import contextmanager


def identity(x):
    return x


def make2d(x):
    if len(x.shape) == 2:
        return x
    elif len(x.shape) == 1:
        return x.reshape(-1, 1)
    else:
        raise ValueError("Can't make 2d array with shape %s" % str(x.shape))


def hstack(x, dtype=None):
    ne = 0
    nnz = 0

    if dtype is not None:
        x = [e.astype(dtype) if e.dtype != dtype else e for e in x]

    # Count number of elements and non-zeros
    for e in x:
        ne += np.product(e.shape)
        nnz += e.nnz if sp.issparse(e) else np.product(e.shape)

    # Too many non-zero entries, cast to dense
    if nnz > 0.5 * ne:
        x = [e.toarray() if sp.issparse(e) else e for e in x]

    if any(sp.issparse(p) for p in x):
        return sp.hstack(x, format='csr')
    else:
        return np.hstack(x)


def vstack(x):
    if any(sp.issparse(p) for p in x):
        return sp.vstack(x, format='csr')
    else:
        return np.vstack(x)


def save_pickle(filename, data):
    with open(filename, 'wb') as f:
        pickle.dump(data, f)


def load_pickle(filename):
    with open(filename, 'rb') as f:
        return pickle.load(f)


def call_with(method, *arg_series):
    args = []
    kwargs = {}

    for a in arg_series:
        if isinstance(a, (list, tuple)):
            assert len(kwargs) == 0
            args += list(a)
        elif isinstance(a, dict):
            for k, v in a.items():
                assert k not in kwargs
                kwargs[k] = v
        else:
            raise RuntimeError("Unknown arg serie: %r" % a)

    return method(*args, **kwargs)


@contextmanager
def timer(text):
    print(text + '... ', flush=True, end='')
    start_time = time()
    yield
    print("done in %d seconds" % (time() - start_time), flush=True)
