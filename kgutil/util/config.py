import threading

from contextlib import contextmanager

from attrdict.mixins import Attr
from attrdict.merge import merge


class ConfigSection(Attr):

    def __init__(self, contents):
        self._contents = contents

    def __getitem__(self, key):
        return self.__dict__['_contents'][key]

    #def __contains__(self, key):
    #    return key in self.__contents

    def __iter__(self):
        return iter(self._contents)

    def __len__(self):
        return len(self._contents)

    @classmethod
    def _constructor(cls, contents, _cfg):
        return cls(contents)

    def _configuration(self):
        return None


class Config(Attr):

    def __init__(self):
        self.__global_config = {}
        self.__tls = threading.local()

    def update_default(self, **kws):
        self.__global_config = merge(kws, self.__global_config)

    def update(self, **kws):
        self.__global_config = merge(self.__global_config, kws)

    def all(self):
        return self.__tls_config() or self.__global_config

    def copy(self):
        return ConfigSection(self.all())

    @contextmanager
    def scoped(self, **kws):
        old_tls_config = self.__tls_config()

        if old_tls_config is not None:
            self.__tls.config = merge(old_tls_config, kws)
        else:
            self.__tls.config = merge(self.__global_config, kws)

        yield

        self.__tls.config = old_tls_config

    def __getitem__(self, key):
        return self.all()[key]

    def __contains__(self, key):
        return key in self.all()

    def __iter__(self):
        return iter(self.all())

    def __len__(self):
        return len(self.all())

    @classmethod
    def _constructor(cls, contents, _cfg):
        return ConfigSection(contents)

    def _configuration(self):
        return None

    def __tls_config(self):
        return getattr(self.__tls, 'config', None)
