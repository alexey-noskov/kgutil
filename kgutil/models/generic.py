from __future__ import print_function

import numpy as np

from copy import deepcopy

from sklearn.utils import resample
from sklearn.model_selection import train_test_split


class Averaged(object):

    def __init__(self, n_runs, model_proto, sample=False, sample_replace=True, geom=False, val_size=None, score=None):
        self.n_runs = n_runs
        self.model_proto = model_proto
        self.sample = sample
        self.sample_replace = sample_replace
        self.geom = geom
        self.val_size = val_size
        self.score = score
        self.run_models = None

    def fit_predict(self, train_X, train_y, test_X, test_y, seed, context):
        self.run_models = []

        test_p = None

        for i in xrange(self.n_runs):
            print("      Run %d..." % i)

            run_train_X = train_X
            run_train_y = train_y

            if self.sample:
                if type(self.sample) is float:
                    n_samples = int(self.sample * train_X.shape[0])
                elif type(self.sample) is int:
                    n_samples = self.sample
                else:
                    n_samples = train_X.shape[0]

                idx = resample(range(train_X.shape[0]), n_samples=n_samples, replace=self.sample_replace, random_state=seed)
                run_train_X = run_train_X.iloc[idx]
                run_train_y = run_train_y.iloc[idx]

            run_model = deepcopy(self.model_proto)

            # No special validation set, fit model as is
            if self.val_size is None:
                run_test_p = run_model.fit_predict(run_train_X, run_train_y, test_X, test_y, seed=seed, context=context.child('run-%s' % i))
            else:
                run_train_X, run_eval_X, run_train_y, run_eval_y = train_test_split(run_train_X, run_train_y, test_size=self.val_size, random_state=seed+11)

                run_model.fit_eval(run_train_X, run_train_y, run_eval_X, run_eval_y, seed=seed, context=context.child('run-%s' % i))

                if self.score is not None:
                    run_eval_p = run_model.predict(run_eval_X)
                    run_scores = self.score(run_eval_y, run_eval_p)

                    if not isinstance(run_scores, dict):
                        run_scores = {'score': run_scores}

                    for k, v in run_scores.items():
                        print("      %s: %.6f" % (k, v))

                run_test_p = run_model.predict(test_X)

            self.run_models.append(run_model)

            if self.geom:
                run_test_p = np.log(np.clip(run_test_p, 1e-7, 1.0))

            if test_p is None:
                test_p = run_test_p
            else:
                test_p += run_test_p

            # Change seed for next run
            seed = (seed * 131071 + i * 110503) % 253450711

        test_p /= self.n_runs

        if self.geom:
            test_p = np.exp(test_p)

        return test_p

    def predict(self, X):
        preds = None

        for run_model in self.run_models:
            run_preds = run_model.predict(X)

            if self.geom:
                run_preds = np.log(np.clip(run_preds, 1e-7, 1.0))

            if preds is None:
                preds = run_preds
            else:
                preds += run_preds

        preds /= self.n_runs

        if self.geom:
            preds = np.exp(preds)

        return preds


class MultiLabel(object):

    def __init__(self, model_proto):
        self.model_proto = model_proto
        self.lbl_models = None

    def fit_predict(self, train_X, train_y, test_X, test_y, seed, context):
        self.labels = self._get_labels(train_y)
        self.lbl_models = []

        preds = []
        for li, lbl in enumerate(self.labels):
            lbl_model = deepcopy(self.model_proto)
            lbl_preds = lbl_model.fit_predict(train_X, train_y.iloc[:, li], test_X, test_y.iloc[:, li] if test_y is not None else None, seed=seed, context=context.child('lbl-%s' % lbl))

            self.lbl_models.append(lbl_model)
            preds.append(lbl_preds)

        return self._combine_preds(preds)

    def predict(self, X):
        preds = []
        for lbl_model in self.lbl_models:
            preds.append(lbl_model.predict(X))

        return self._combine_preds(preds)

    def _get_labels(self, y):
        if hasattr(y, 'columns'):
            return list(y.columns)
        else:
            return range(y.shape[1])

    def _combine_preds(self, preds):
        if len(preds[0].shape) == 1:
            return np.stack(preds, axis=-1)
        else:
            return np.concatenate(preds, axis=-1)
