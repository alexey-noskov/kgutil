import os
import xgboost as xg

from .. import config


config.update_default(
    xgb=dict(
        objective='reg:linear',
        eval_metric='rmse',
        silent=1,
        nthread=-1,
        eta=0.1,
        max_depth=6,
        num_boost_round=50,
        verbose_eval=10
    )
)


class Xgb(object):

    def __init__(self, **opts):
        self.options = config.xgb + opts

    def fit_predict(self, train_X, train_y, test_X, test_y, seed, context):
        param, num_boost_round, kwa = self.build_options(train_X.shape[0], seed, train_X, train_y)

        xgtrain = xg.DMatrix(train_X.values, label=train_y.values, feature_names=train_X.columns)

        if test_y is not None:
            xgtest = xg.DMatrix(test_X.values, label=test_y.values, feature_names=test_X.columns)
            watchlist = [(xgtrain, 'train'), (xgtest, 'val')]
        else:
            xgtest = xg.DMatrix(test_X.values, feature_names=test_X.columns)
            watchlist = [(xgtrain, 'train')]

        self.model = xg.train(list(param.items()), xgtrain, num_boost_round, watchlist, **kwa)

        if context.debug_dir is not None:
            if not os.path.exists(context.debug_dir):
                os.makedirs(context.debug_dir)

            # Write model dump
            self.model.dump_model(os.path.join(context.debug_dir, 'xgb.dump'), with_stats=True)

            # Write feature importances
            importances = self.model.get_score(importance_type='gain')
            with open(os.path.join(context.debug_dir, 'importances.txt'), 'w') as out:
                for f, s in sorted(importances.items(), key=lambda t: -t[1]):
                    out.write('%.3f:%s\n' % (s, f))

        return self.model.predict(xgtest)

    def predict(self, X):
        xgtest = xg.DMatrix(X.values, feature_names=X.columns)

        return self.model.predict(xgtest)

    def set_params(self, **kw):
        for k, v in kw.items():
            self.options[k] = v

    def build_options(self, train_size, seed, X, y):
        opts = dict(self.options)
        opts['seed'] = seed

        num_boost_round = opts.pop('num_boost_round')

        if 'base_train_size' in opts:
            base_train_size = opts.pop('base_train_size')
            num_boost_round = num_boost_round * train_size // base_train_size

        kwa = {}
        for key in ['verbose_eval', 'early_stopping_rounds', 'feval']:
            if key in opts:
                kwa[key] = opts.pop(key)

        for key in ['scale_pos_weight', 'base_score']:
            if key in opts and callable(opts[key]):
                opts[key] = opts[key](X, y)

        return opts, num_boost_round, kwa
