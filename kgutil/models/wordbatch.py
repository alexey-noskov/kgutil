import lazy_import
import numpy as np
from kgutil.data import iter_df_batches

wordbatch = lazy_import.lazy_module('wordbatch')


__all__ = ['WordbatchModel']


class WordbatchModel:

    def __init__(self, model, preprocessor, extractor, batch_size):
        self.model = model
        self.preprocessor = preprocessor
        self.extractor = extractor
        self.batch_size = batch_size

    def fit_eval(self, train_X, train_y, eval_X, eval_y):
        if callable(train_X):
            train_X = train_X()
        if callable(train_y):
            train_y = train_y()

        self.wb = wordbatch.WordBatch(
            None,
            extractor=self.extractor,
            minibatch_size=self.batch_size // 80,
            procs=8, freeze=True, timeout=1800, verbose=0)

        for batch_X, batch_y in iter_df_batches(train_X, train_y, batch_size=self.batch_size):
            if self.preprocessor is not None:
                batch_X, batch_y, batch_w = self.preprocessor(batch_X, batch_y)

            batch_X = self.wb.transform(batch_X)

            self.model.partial_fit(batch_X, batch_y, sample_weight=batch_w)

    def predict(self, X):
        if callable(X):
            X = X()

        res = []
        for batch_X in iter_df_batches(X, batch_size=self.batch_size):
            if self.preprocessor is not None:
                batch_X = self.preprocessor(batch_X)

            batch_X = self.wb.transform(batch_X)

            res.append(self.model.predict(batch_X))

        return np.concatenate(res, axis=0)
