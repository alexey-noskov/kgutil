import lazy_import
import os
import contextlib
import numpy as np

from kgutil.data import iter_df_batches
from kgutil.util import timer

batch_learn = lazy_import.lazy_module('batch_learn')


__all__ = ['BatchLearnModel']


class BatchLearnModel:

    def __init__(self, preprocessor=None, cache_dir='tmp/batch_learn', batch_size=10000, index_bits=20):
        self.cache_dir = cache_dir
        self.preprocessor = preprocessor
        self.batch_size = batch_size
        self.index_bits = index_bits

    def fit_eval(self, train_X, train_y, eval_X, eval_y):
        if callable(train_X):
            train_X = train_X()
        if callable(train_y):
            train_y = train_y()

        self._export_file('train', train_X, train_y)

        if eval_X is not None and eval_y is not None:
            if callable(eval_X):
                eval_X = eval_X()
            if callable(eval_y):
                eval_y = eval_y()
            self._export_file('eval', eval_X, eval_y)

    def predict(self, X):
        if callable(X):
            X = X()

        res = []
        for batch_X in iter_df_batches(X, batch_size=self.batch_size):
            if self.preprocessor is not None:
                batch_X = self.preprocessor(batch_X)

            res.append(np.zeros(batch_X.shape[0]))

        return np.concatenate(res, axis=0)

    def _export_file(self, filename, X, y):
        cache_filename = os.path.join(self.cache_dir, filename)
        if not os.path.exists(self.cache_dir):
            os.makedirs(self.cache_dir)
        if os.path.exists(cache_filename + '.index'):
            os.remove(cache_filename + '.index')
        if os.path.exists(cache_filename + '.data'):
            os.remove(cache_filename + '.data')

        with timer("Writing %s" % cache_filename):
            with contextlib.closing(batch_learn.Writer(cache_filename, self.index_bits)) as writer:
                for batch_X, batch_y in iter_df_batches(X, y, batch_size=self.batch_size):
                    if self.preprocessor is not None:
                        batch_X = self.preprocessor(batch_X, batch_y)

                    labels = np.asarray(batch_y) * 2 - 1  # Convert 0/1 targets to -1/1
                    indices = batch_X.values.astype(np.uint32)
                    fields = np.asarray(range(batch_X.shape[1]), dtype=np.uint32)
                    values = np.ones(batch_X.shape[1], dtype=np.uint32)
                    for i in range(batch_X.shape[0]):
                        writer.write_row(fields, indices[i], values, labels[i])

                writer.write_index()
