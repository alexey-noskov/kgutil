from __future__ import print_function

import os

from sklearn.preprocessing import StandardScaler

from .. import config


config.update_default(
    sklearn=dict(
        scale=False,
        preprocess=None,
        method='predict'
    )
)


class Sklearn(object):

    def __init__(self, estimator, **opts):
        self.estimator = estimator
        self.opts = config.sklearn + opts

    def set_params(self, **kw):
        self.estimator.set_params(**kw)

    def fit_predict(self, train_X, train_y, test_X, test_y, seed, context):
        columns = train_X.columns

        train_X = train_X.values
        train_y = train_y.values

        test_X = test_X.values
        test_y = test_y.values if test_y is not None else test_y

        if 'random_state' in self.estimator.get_params():
            self.estimator.set_params(random_state=seed)

        if self.opts.preprocess is not None:
            train_X = self.preprocess(train_X)
            test_X = self.preprocess(test_X)

        if self.opts.scale:
            scaler = StandardScaler(with_mean=False)

            train_X = scaler.fit_transform(train_X)
            test_X = scaler.transform(test_X)

        self.estimator.fit(train_X, train_y)

        if test_y is not None and hasattr(self.estimator, 'staged_predict'):
            best_score = 1e9
            best_i = -1
            for i, test_p in enumerate(getattr(self.estimator, 'staged_' + self.opts.method)(test_X)):
                score = config.models.score(test_y, test_p)

                if score < best_score:
                    best_score = score
                    best_i = i

            print("Best score: %.5f after %d iters" % (best_score, best_i))

        if context.debug_dir is not None:
            if not os.path.exists(context.debug_dir):
                os.makedirs(context.debug_dir)

            if hasattr(self.estimator, 'coef_'):
                coef = self.estimator.coef_

                if len(coef.shape) > 1:
                    coef = coef[0]

                with open(os.path.join(context.debug_dir, 'coef.txt'), 'w') as out:
                    for w, f in reversed(sorted(zip(coef, columns))):
                        out.write('%.3f:%s\n' % (w, f))

        return self.predict(test_X)

    def predict(self, X):
        return getattr(self.estimator, self.opts.method)(X)
