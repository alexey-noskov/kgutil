import numpy as np
import scipy.sparse as sp

import os

from .. import config

from sklearn.preprocessing import StandardScaler


from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers.advanced_activations import PReLU
from keras.layers.normalization import BatchNormalization
from keras.utils.np_utils import to_categorical
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras import regularizers


config.update_default(
    keras=dict(
        optimizer='adadelta',
        loss='binary_crossentropy',
        batch_size=32,
        predict_batch_size=512,
        out_activation='sigmoid',
        class_weight=None,
        early_stop=None,
    )
)


class Keras(object):

    def __init__(self, arch, **opts):
        self.arch = arch
        self.opts = config.keras + opts

    def fit_predict(self, train_X, train_y, test_X, test_y, seed, context):
        opts = self.opts

        train_X = train_X.values
        train_y = train_y.values

        test_X = test_X.values

        if test_y is not None:
            test_y = test_y.values

        if 'preprocess' in opts:
            train_X = opts['preprocess'](train_X)
            test_X = opts['preprocess'](test_X)

        if opts.get('scale', True):
            self.scaler = StandardScaler(with_mean=False)

            train_X = self.scaler.fit_transform(train_X)
            test_X = self.scaler.transform(test_X)

        self._fit_internal(train_X, train_y, test_X, test_y, seed, context)

        return self.model.predict_generator(batch_generator(test_X, batch_size=opts.predict_batch_size), steps=(test_X.shape[0] + opts.predict_batch_size - 1) // opts.predict_batch_size)

    def fit_eval(self, train_X, train_y, eval_X, eval_y, seed, context):
        train_X = train_X.values
        train_y = train_y.values

        eval_X = eval_X.values
        eval_y = eval_y.values

        if 'preprocess' in self.opts:
            train_X = self.opts['preprocess'](train_X)
            eval_X = self.opts['preprocess'](eval_X)

        if self.opts.get('scale', True):
            self.scaler = StandardScaler(with_mean=False)

            train_X = self.scaler.fit_transform(train_X)
            eval_X = self.scaler.transform(eval_X)

        self._fit_internal(train_X, train_y, eval_X, eval_y, seed, context)

    def predict(self, test_X):
        test_X = test_X.values

        if 'preprocess' in self.opts:
            test_X = self.opts['preprocess'](test_X)

        if self.opts.get('scale', True):
            test_X = self.scaler.transform(test_X)

        return self.model.predict_generator(batch_generator(test_X, batch_size=self.opts.predict_batch_size), steps=(test_X.shape[0] + self.opts.predict_batch_size - 1) // self.opts.predict_batch_size)

    def _fit_internal(self, train_X, train_y, eval_X, eval_y, seed, context):
        if not os.path.exists(context.debug_dir):
            os.makedirs(context.debug_dir)

        model_weights_path = os.path.join(context.debug_dir, 'model-weights.h5')
        model_arch_path = os.path.join(context.debug_dir, "model.json")

        opts = self.opts
        n_classes = self.opts.get('n_classes')

        np.random.seed(seed)

        callbacks = []
        if opts.early_stop is not None:
            callbacks.append(EarlyStopping(monitor='val_loss', patience=opts.early_stop))
            callbacks.append(ModelCheckpoint(model_weights_path, save_best_only=True, save_weights_only=True))
        else:
            callbacks.append(ModelCheckpoint(model_weights_path, save_weights_only=True))

        self.model = self.arch((train_X.shape[1],), opts)
        self.model.compile(optimizer=opts.optimizer, loss=opts.loss)

        # Save model architecture to json
        with open(model_arch_path, "w") as f:
            f.write(self.model.to_json())

        # Train model
        self.model.fit_generator(
            generator=batch_generator(train_X, train_y, opts.batch_size, shuffle=True, n_classes=n_classes, class_weight=opts.class_weight),
            steps_per_epoch=(train_X.shape[0] + opts.batch_size - 1) // opts.batch_size,
            validation_data=batch_generator(eval_X, eval_y, opts.predict_batch_size, n_classes=n_classes, class_weight=opts.class_weight) if eval_y is not None else None,
            validation_steps=(eval_X.shape[0] + opts.predict_batch_size - 1) // opts.predict_batch_size if eval_y is not None else None,
            epochs=opts['n_epoch'], verbose=1, callbacks=callbacks)

        # Restore from best checkpoint if early_stop enabled
        if opts.early_stop is not None:
            self.model.load_weights(model_weights_path)


def regularizer(params):
    if 'l1' in params and 'l2' in params:
        return regularizers.l1l2(params['l1'], params['l2'])
    elif 'l1' in params:
        return regularizers.l1(params['l1'])
    elif 'l2' in params:
        return regularizers.l2(params['l2'])
    else:
        return None


def mlp(input_shape, params):
    model = Sequential()

    if 'input_dropout' in params:
        model.add(Dropout(params['input_dropout'], input_shape=input_shape))

    for i, layer_size in enumerate(params['layers']):
        reg = regularizer(params)

        if i == 0 and 'input_dropout' not in params:
            model.add(Dense(layer_size, kernel_initializer='glorot_normal', kernel_regularizer=reg, input_shape=input_shape))
        else:
            model.add(Dense(layer_size, kernel_initializer='glorot_normal', kernel_regularizer=reg))

        model.add(PReLU())

        if params.get('batch_norm', False):
            model.add(BatchNormalization())

        if 'dropouts' in params:
            model.add(Dropout(params['dropouts'][i]))

    if len(params['layers']) > 0:
        model.add(Dense(params.get('n_classes', 1), activation=params.out_activation, kernel_initializer='glorot_normal'))
    else:
        model.add(Dense(params.get('n_classes', 1), activation=params.out_activation, kernel_initializer='glorot_normal', input_shape=input_shape))

    return model


def random_mlp(input_shape, params):
    model = Sequential()

    if 'input_dropout' in params:
        model.add(Dropout(params['input_dropout'], input_shape=input_shape))

    for i, layer_size in enumerate(params['layers']):
        reg = regularizer(params)
        layer_size = int(np.random.uniform(0.5*layer_size, 2.0*layer_size))

        if i == 0 and 'input_dropout' not in params:
            model.add(Dense(layer_size, kernel_initializer='glorot_normal', kernel_regularizer=reg, input_shape=input_shape))
        else:
            model.add(Dense(layer_size, kernel_initializer='glorot_normal', kernel_regularizer=reg))

        model.add(PReLU())

        if params.get('batch_norm', False):
            model.add(BatchNormalization())

        if 'dropouts' in params:
            model.add(Dropout(np.random.uniform(0.5*params['dropouts'][i], 2.0*params['dropouts'][i])))

    if len(params['layers']) > 0:
        model.add(Dense(params.get('n_classes', 1), activation=params.out_activation, kernel_initializer='glorot_normal'))
    else:
        model.add(Dense(params.get('n_classes', 1), activation=params.out_activation, kernel_initializer='glorot_normal', input_shape=input_shape))

    return model


def batch_generator(X, y=None, batch_size=128, shuffle=False, n_classes=None, class_weight=None):
    index = np.arange(X.shape[0])

    while True:
        if shuffle:
            np.random.shuffle(index)

        batch_start = 0
        while batch_start < X.shape[0]:
            batch_index = index[batch_start:batch_start + batch_size]
            batch_start += batch_size

            X_batch = X[batch_index]

            if sp.issparse(X_batch):
                X_batch = X_batch.toarray()

            if y is None:
                yield X_batch
            else:
                y_batch = y[batch_index]

                if n_classes is not None:
                    y_batch = to_categorical(y_batch, n_classes)

                if class_weight is not None:
                    weight_batch = np.array([class_weight[v] for v in y_batch])

                    yield X_batch, y_batch, weight_batch
                else:
                    yield X_batch, y_batch
