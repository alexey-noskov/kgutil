from ..base import BaseModel
from ...util import save_pickle, load_pickle

from keras.callbacks import Callback, EarlyStopping
from keras.utils import Sequence
from keras.models import load_model

import numpy as np


class ExternalMetricsCallback(Callback):

    def __init__(self, predictor, metrics, frequency, eval_X, eval_y):
        super().__init__()

        self.predictor = predictor
        self.metrics = metrics
        self.metrics_history = []
        self.frequency = frequency
        self.eval_X = eval_X
        self.eval_y = eval_y

    def on_epoch_end(self, epoch, logs={}):
        if epoch % self.frequency != 0:
            return

        pred = self.predictor.predict(self.eval_X)

        metric_values = {}
        for name, fn in self.metrics.items():
            metric_values[name] = fn(self.eval_y, pred)

        print("\nMetrics: {}\n".format(metric_values))

        self.metrics_history.append(metric_values)


class DefaultTrainSequence(Sequence):

    def __init__(self, data_transformer, target_transformer, X, y, batch_size):
        self.data_transformer = data_transformer
        self.target_transformer = target_transformer
        self.X = X
        self.y = y
        self.batch_size = batch_size

        self.index = np.random.permutation(len(self.X))
        self.length = (len(self.X) + self.batch_size - 1) // self.batch_size

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        batch_x = self.X.iloc[self.index[idx * self.batch_size:(idx + 1) * self.batch_size]]
        batch_y = self.y.iloc[self.index[idx * self.batch_size:(idx + 1) * self.batch_size]]

        return self._transform_batch(batch_x, batch_y)

    def on_epoch_end(self):
        self.index = np.random.permutation(len(self.X))

    def _transform_batch(self, batch_x, batch_y):
        batch_x = self.data_transformer.transform(batch_x)
        batch_y = self.target_transformer.transform(batch_y)
        return batch_x, batch_y


class DefaultTestSequence(Sequence):

    def __init__(self, data_transformer, X, batch_size):
        self.data_transformer = data_transformer
        self.X = X
        self.batch_size = batch_size

        self.length = (len(self.X) + self.batch_size - 1) // self.batch_size

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        batch_x = self.X.iloc[idx * self.batch_size:(idx + 1) * self.batch_size]

        return self._transform_batch(batch_x)

    def _transform_batch(self, batch_x):
        return self.data_transformer.transform(batch_x)


class BaseKerasModel(BaseModel):

    def __init__(
        self,
        num_epochs=None, batch_size=None, compile_opts=None, external_metrics={}, external_metrics_freq=1,
        early_stopping_opts=None, predict_batch_size=None, predict_passes=1, train_schedule=None,
    ):
        if train_schedule is not None:
            assert num_epochs is None
            self.train_schedule = train_schedule
        else:
            self.train_schedule = [{'num_epochs': num_epochs}]

        self.batch_size = batch_size

        self.predict_batch_size = batch_size if predict_batch_size is None else predict_batch_size
        self.predict_passes = predict_passes
        self.compile_opts = compile_opts

        self.external_metrics = external_metrics
        self.external_metrics_freq = external_metrics_freq

        self.early_stopping_opts = early_stopping_opts

    def fit(self, train_X, train_y):
        self.fit_eval(train_X, train_y, None, None)
        return self

    def fit_eval(self, train_X, train_y, eval_X, eval_y):
        self.data_transformer = self._build_data_transformer()
        self.target_transformer = self._build_target_transformer()

        # Transform train data and targets
        self.data_transformer.fit(train_X, train_y)
        self.target_transformer.fit(train_y)

        callbacks = []
        metrics_history = []

        # Remember target dimensions
        self.target_shape = self.target_transformer.transform(train_y.iloc[:1]).shape[1:]

        # Transform evaluation data if present
        if eval_X is not None:
            eval_X_tr = self.data_transformer.transform(eval_X)
            eval_y_tr = self.target_transformer.transform(eval_y)

            if len(self.external_metrics) > 0:
                cb = ExternalMetricsCallback(self, self.external_metrics, self.external_metrics_freq, eval_X, eval_y)
                metrics_history = cb.metrics_history
                callbacks.append(cb)

        if self.early_stopping_opts is not None:
            callbacks.append(EarlyStopping(**self.early_stopping_opts))

        # Build model
        self.model = self._build_model()
        if self.compile_opts is not None:
            self.model.compile(**self.compile_opts)

        for stage in self.train_schedule:
            num_epochs = stage['num_epochs']
            batch_size = stage.get('batch_size', self.batch_size)

            train_seq = self._build_train_sequence(train_X, train_y, batch_size)

            self.model.fit_generator(
                train_seq, epochs=num_epochs,
                validation_data=(eval_X_tr, eval_y_tr) if eval_X is not None else None,
                callbacks=callbacks, verbose=1)

        self.model.save('model-weights.h5')

        return metrics_history

    def predict(self, X):
        seq = self._build_test_sequence(X, self.predict_batch_size)
        p_tr = sum(self.model.predict_generator(seq) for _ in range(self.predict_passes)) / self.predict_passes
        return self.target_transformer.inverse_transform(p_tr)

    def save(self, filename):
        self.model.save(filename + '.model.h5')
        save_pickle(filename + '.data_transformer.pickle', self.data_transformer)
        save_pickle(filename + '.target_transformer.pickle', self.target_transformer)

    def load(self, filename):
        self.model = load_model(filename + '.model.h5')
        self.data_transformer = load_pickle(filename + '.data_transformer.pickle')
        self.target_transformer = load_pickle(filename + '.target_transformer.pickle')

    def _build_model(self):
        raise NotImplementedError

    def _build_data_transformer(self):
        raise NotImplementedError

    def _build_target_transformer(self):
        raise NotImplementedError

    def _build_train_sequence(self, X, y, batch_size):
        return DefaultTrainSequence(self.data_transformer, self.target_transformer, X, y, batch_size)

    def _build_test_sequence(self, X, batch_size):
        return DefaultTestSequence(self.data_transformer, X, batch_size)
