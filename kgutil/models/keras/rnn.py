import pandas as pd
import numpy as np

from sklearn.base import BaseEstimator
from sklearn.preprocessing import FunctionTransformer, StandardScaler

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

from .base import BaseKerasModel
from ...util import identity


def slice_last(x):
    return x[..., -1, :]


def load_emb_matrix(filename, word_index, voc_size, emb_size, rand_std=None):
    if rand_std is None:
        emb_matrix = np.zeros((voc_size, emb_size), dtype=np.float32)
    else:
        emb_matrix = np.random.randn(voc_size, emb_size) * rand_std

    print("Loading embeddings...")
    for line in open(filename, 'r'):
        values = line.split()

        if len(values) != emb_size + 1:
            continue

        idx = word_index.get(values[0])
        if idx is not None and idx < voc_size:
            emb_matrix[idx] = np.asarray(values[1:], dtype=np.float32)

    return emb_matrix


def apply_rnn(x, cell_type, cell_size, bidi=False, return_sequences=False, use_cudnn=False, dropout=None, residual=False):
    from keras.layers import GRU, LSTM, CuDNNGRU, CuDNNLSTM, Bidirectional, Dropout, SpatialDropout1D, Conv1D, Lambda, add

    if cell_type == 'gru':
        cell_cls = CuDNNGRU if use_cudnn else GRU
    elif cell_type == 'lstm':
        cell_cls = CuDNNLSTM if use_cudnn else LSTM
    else:
        raise RuntimeError("Unknown cell type %r" % cell_type)

    cell = cell_cls(cell_size, return_sequences=return_sequences)

    if bidi:
        cell = Bidirectional(cell)

    x_inp = x
    x = cell(x)

    if dropout is not None:
        if return_sequences:
            x = SpatialDropout1D(dropout)(x)
        else:
            x = Dropout(dropout)(x)

    if residual:
        if x.shape[-1] != x_inp.shape[-1]:
            x_inp = Conv1D(cell_size, kernel_size=1)(x_inp)

        if not return_sequences:
            # Last layer does not return sequences, just the last element
            # so we select only the last element of the previous output.
            x_inp = Lambda(slice_last)(x_inp)

        x = add([x, x_inp])

    return x


def apply_pooling_1d(x, pooling_type):
    from keras.layers import GlobalAveragePooling1D, GlobalMaxPooling1D, concatenate

    if pooling_type == 'avgmax':
        avg_pool = GlobalAveragePooling1D()(x)
        max_pool = GlobalMaxPooling1D()(x)
        return concatenate([avg_pool, max_pool])
    elif pooling_type == 'avg':
        return GlobalAveragePooling1D()(x)
    elif pooling_type == 'max':
        return GlobalMaxPooling1D()(x)
    elif pooling_type is None:
        return x
    else:
        raise RuntimeError("Unknown pooling: %r" % pooling_type)


def build_rnn_model(
    data, target_shape,
    mlp_layers=[64], mlp_dropout=0.1,
    rnn_layers=[16], rnn_bidi=False, rnn_pooling=None, rnn_cell='gru', rnn_cudnn=False, rnn_dropout=None, rnn_residual=False,
    text_emb_size=32, text_emb_file=None, text_emb_trainable=True, text_emb_dropout=None, text_emb_rand_std=None,
    out_activation='linear'
):
    from keras.layers import Input, Dropout, Dense, concatenate, Embedding, SpatialDropout1D
    from keras.models import Model

    # Load embeddings if provided
    if text_emb_file is None:
        emb_layer = Embedding(data.text_voc_size, text_emb_size, trainable=text_emb_trainable)
    else:
        emb_matrix = load_emb_matrix(text_emb_file, data.text_tokenizer.word_index, data.text_voc_size, text_emb_size)
        emb_layer = Embedding(data.text_voc_size, text_emb_size, weights=[emb_matrix], trainable=text_emb_trainable)

    # Inputs
    inputs = []
    parts = []

    # Text columns
    for col in data.text_columns:
        inp = Input(shape=[data.max_text_len], name=col)

        x = emb_layer(inp)
        if text_emb_dropout is not None:
            x = SpatialDropout1D(text_emb_dropout)(x)

        for i, layer_size in enumerate(rnn_layers):
            x = apply_rnn(
                x, rnn_cell, layer_size,
                bidi=rnn_bidi, use_cudnn=rnn_cudnn, dropout=rnn_dropout, residual=rnn_residual,
                return_sequences=(rnn_pooling is not None or i < len(rnn_layers) - 1)
            )

        x = apply_pooling_1d(x, rnn_pooling)

        inputs.append(inp)
        parts.append(x)

    # Numeric columns
    if len(data.numeric_columns) > 0:
        inp = Input(shape=[len(data.numeric_columns)], name="numeric_columns__")
        inputs.append(inp)
        parts.append(inp)

    # MLP
    out = concatenate(parts) if len(parts) > 1 else parts[0]

    for layer_size in mlp_layers:
        if mlp_dropout is not None:
            out = Dropout(mlp_dropout)(out)
        out = Dense(layer_size, activation='relu')(out)

    if mlp_dropout is not None:
        out = Dropout(mlp_dropout)(out)

    # Output
    out = Dense(target_shape[0] if len(target_shape) > 0 else 1, activation=out_activation)(out)

    # model
    return Model(inputs, out)


class KerasRNNDataTransformer(BaseEstimator):

    def __init__(self, num_text_words=None, max_text_len=None, text_padding='pre', text_truncating='pre', text_tokenizer_opts={}, ignore_columns=[]):
        self.num_text_words = num_text_words
        self.max_text_len = max_text_len
        self.text_padding = text_padding
        self.text_truncating = text_truncating
        self.text_tokenizer_opts = text_tokenizer_opts
        self.ignore_columns = ignore_columns

    def fit_transform(self, X, y):
        self.fit(X, y)
        return self.transform(X)

    def fit(self, X, y):
        self._detect_columns(X)

        if len(self.text_columns) > 0:
            self.text_tokenizer = Tokenizer(num_words=self.num_text_words, **self.text_tokenizer_opts)
            self.text_tokenizer.fit_on_texts(pd.concat([X[col] for col in self.text_columns]).map(str).values)

            if self.num_text_words is not None:
                self.text_voc_size = self.num_text_words + 1
            else:
                self.text_voc_size = len(self.text_tokenizer.word_index) + 1

            print("Voc size: %d" % self.text_voc_size)

        if len(self.numeric_columns) > 0:
            self.numeric_scaler = StandardScaler().fit(X[self.numeric_columns].values)

        return self

    def transform(self, X):
        dataset = {}
        for col in self.text_columns:
            dataset[col] = pad_sequences(self.text_tokenizer.texts_to_sequences(X[col]), maxlen=self.max_text_len, padding=self.text_padding, truncating=self.text_truncating)

        if len(self.numeric_columns) > 0:
            numeric_values = self.numeric_scaler.transform(X[self.numeric_columns].values)
            for idx, col in enumerate(self.numeric_columns):
                dataset[col] = numeric_values[:, idx]
            dataset['numeric_columns__'] = numeric_values

        return dataset

    def _detect_columns(self, X):
        self.text_columns = []
        self.numeric_columns = []

        for col in X.columns:
            if col in self.ignore_columns:
                continue

            dtype = X.dtypes[col]

            if dtype.kind in ['i', 'u', 'f']:
                self.numeric_columns.append(col)
            elif dtype == np.object and isinstance(X[col].iloc[0], str):
                self.text_columns.append(col)
            else:
                raise RuntimeError("Unknown column type: {}" % dtype)

        print("Text columns: {}".format(self.text_columns))
        print("Numeric columns: {}".format(self.numeric_columns))


class KerasRNN(BaseKerasModel):

    def __init__(
        self,
        _sentinel=None,
        num_text_words=None, max_text_len=100, text_padding='pre', text_truncating='pre', text_tokenizer_opts={}, ignore_columns=[],
        model_fn=build_rnn_model, model_opts={},
        **kwargs
    ):
        super().__init__(**kwargs)
        self.num_text_words = num_text_words
        self.max_text_len = max_text_len
        self.text_padding = text_padding
        self.text_truncating = text_truncating
        self.text_tokenizer_opts = text_tokenizer_opts
        self.ignore_columns = ignore_columns
        self.model_fn = model_fn
        self.model_opts = model_opts

    def _build_model(self):
        return self.model_fn(self.data_transformer, self.target_shape, **self.model_opts)

    def _build_data_transformer(self):
        return KerasRNNDataTransformer(
            self.num_text_words, self.max_text_len, self.text_padding, self.text_truncating, self.text_tokenizer_opts,
            ignore_columns=self.ignore_columns)

    def _build_target_transformer(self):
        return FunctionTransformer(identity, identity, validate=False)
