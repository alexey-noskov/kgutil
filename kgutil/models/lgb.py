import lightgbm as lg

from .. import config

config.update_default(
    lgb=dict(
        boosting_type='gbdt',
        objective='binary',
        metric='binary_logloss',
        num_leaves=31,
        learning_rate=0.05,
        feature_fraction=0.9,
        bagging_fraction=0.8,
        bagging_freq=5,
        verbose=0,
    )
)


class Lgb(object):

    def __init__(self, categorical_names=None, **opts):
        self.options = config.lgb + opts
        self.categorical_names = categorical_names

    def fit_predict(self, train_X, train_y, test_X, test_y, seed, context):
        params, num_boost_round, kwa = self.build_options(train_X.shape[0], seed)

        feature_names = list(train_X.columns)

        lgtrain = lg.Dataset(train_X.values, train_y.values)
        valid_sets = [lgtrain]
        valid_names = ['train']

        if test_y is not None:
            lgtest = lg.Dataset(test_X.values, test_y.values, reference=lgtrain)
            valid_sets.append(lgtest)
            valid_names.append('eval')
        else:
            lgtest = lg.Dataset(test_X.values, reference=lgtrain)

        model = lg.train(params, lgtrain,
                         num_boost_round=num_boost_round,
                         valid_sets=valid_sets,
                         valid_names=valid_names,
                         feature_name=feature_names,
                         categorical_feature=[feature_names.index(n) for n in self.categorical_names] if self.categorical_names is not None else [],
                         **kwa)

        return model.predict(test_X.values)

    def set_params(self, **kw):
        for k, v in kw.items():
            self.options[k] = v

    def build_options(self, train_size, seed):
        opts = dict(self.options)
        opts['seed'] = seed

        num_boost_round = opts.pop('num_boost_round')

        if 'base_train_size' in opts:
            base_train_size = opts.pop('base_train_size')
            num_boost_round = num_boost_round * train_size // base_train_size

        kwa = {}
        for key in ['verbose_eval', 'early_stopping_rounds', 'feval']:
            if key in opts:
                kwa[key] = opts.pop(key)

        return opts, num_boost_round, kwa
