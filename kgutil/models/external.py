from __future__ import print_function

import pandas as pd
import os
import datetime


class External(object):

    def __init__(self, command, format='csv', strip_namespaces=False):
        self.command = command
        self.format = format
        self.strip_namespaces = strip_namespaces

    def fit_predict(self, train_X, train_y, test_X, test_y, seed, context):
        if self.strip_namespaces is True:
            column_mapping = dict((c, '__'.join(c.split('__')[1:])) for c in train_X.columns)
        else:
            column_mapping = {}

        directory = 'cache/tmp'
        ts = datetime.datetime.now().strftime('%Y%m%d-%H%M')

        if not os.path.exists(directory):
            os.makedirs(directory)

        train_file_name = '%s/%s-train.%s' % (directory, ts, self.format)
        test_file_name = '%s/%s-test.%s' % (directory, ts, self.format)
        pred_file_name = '%s/%s-pred.%s' % (directory, ts, self.format)

        train_X = train_X.rename(columns=column_mapping)
        train_X['y'] = train_y
        self.write_df(train_X, train_file_name)

        self.write_df(test_X.rename(columns=column_mapping), test_file_name)

        command = self.command.format(train=train_file_name, test=test_file_name, pred=pred_file_name, seed=seed)
        print("    Executing '%s'..." % command)
        if os.system(command) != 0:
            raise RuntimeError("Error executing command '%s'" % command)

        return self.read_df(pred_file_name).values

    def write_df(self, df, file_name):
        if self.format == 'csv':
            df.to_csv(file_name, index=False, encoding='utf-8')
        elif self.format == 'pickle':
            df.to_pickle(file_name)
        else:
            raise ValueError("Unsupported format %s" % self.format)

    def read_df(self, file_name):
        if self.format == 'csv':
            return pd.read_csv(file_name, encoding='utf-8')
        elif self.format == 'pickle':
            return pd.read_pickle(file_name)
        else:
            raise ValueError("Unsupported format %s" % self.format)
