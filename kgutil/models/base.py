#from sklearn.base import BaseEstimator


class BaseModel:

    def fit(self, X, y):
        raise NotImplementedError

    def fit_eval(self, X, y, eval_X, eval_y):
        raise NotImplementedError
