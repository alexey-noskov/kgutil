import lazy_import
import attrdict
import gc
import os

from copy import copy
from kgutil.util import save_pickle, load_pickle
from sklearn.preprocessing import FunctionTransformer

lgb = lazy_import.lazy_module('lightgbm')
eli5 = lazy_import.lazy_module('eli5')


__all__ = ['LgbModel']


class LgbModel:

    default_params = {
        'learning_rate': 0.2,
        'application': 'binary',
        'num_leaves': 31,
        'verbosity': -1,
        'metric': 'auc',
        'data_random_seed': 2,
        'bagging_fraction': 0.8,
        'feature_fraction': 0.6,
        'nthread': -1,
        'lambda_l1': 1,
        'lambda_l2': 1
    }

    def __init__(self, num_rounds=30, params={}, preprocessor=None, cache_dir=None, df_opts={}):
        if callable(preprocessor) and not hasattr(preprocessor, 'transform'):
            preprocessor = FunctionTransformer(preprocessor, validate=False)

        self.num_rounds = num_rounds
        self.params = copy(self.default_params)
        self.params.update(params)
        self.preprocessor = preprocessor
        self.cache_dir = cache_dir
        self.df_opts = df_opts

    def fit(self, train_X, train_y):
        return self.fit_eval(train_X, train_y, valid_X=None, valid_y=None)

    def fit_eval(self, train_X, train_y, valid_X, valid_y):
        dtrain = self._prepare_train_data(train_X, train_y)
        valid_sets = [dtrain]

        if valid_X is not None and valid_y is not None:
            dvalid = self._prepare_valid_data(valid_X, valid_y, reference=dtrain)
            valid_sets.append(dvalid)

        self.model = lgb.train(self.params, train_set=dtrain, num_boost_round=self.num_rounds, valid_sets=valid_sets, verbose_eval=1)
        self.model.free_dataset()

    def predict(self, X):
        return self.model.predict(self._prepare_test_data(X))

    def save(self, file_name_prefix):
        save_pickle(file_name_prefix + '-lgb.pickle', self.model)
        if self.preprocessor is not None and not isinstance(self.preprocessor, FunctionTransformer):
            save_pickle(file_name_prefix + '-preproc.pickle', self.preprocessor)

    def load(self, file_name_prefix):
        self.model = load_pickle(file_name_prefix + '-lgb.pickle')
        if self.preprocessor is not None and not isinstance(self.preprocessor, FunctionTransformer):
            self.preprocessor = load_pickle(file_name_prefix + '-preproc.pickle')

    def save_explanation(self, file_name_prefix):
        expl = eli5.lightgbm.explain_weights_lightgbm(attrdict.AttrDict(booster_=self.model))
        with open(file_name_prefix + '.html', 'w') as f:
            f.write(eli5.format_as_html(expl))

    def _prepare_train_data(self, train_X, train_y):
        print("Loading train data...")
        if callable(train_X):
            train_X = train_X()
        if hasattr(train_X, 'to_df'):
            train_X = train_X.to_df(**self.df_opts)
        if callable(train_y):
            train_y = train_y()

        if self.preprocessor is not None:
            print("Preprocessing train data...")
            train_X = self.preprocessor.fit_transform(train_X, train_y)

        feature_names = list(train_X.columns)

        print("Train columns: %s" % ', '.join(feature_names))
        print("Building train dataset...")
        dtrain = lgb.Dataset(train_X, label=train_y, feature_name=feature_names)
        dtrain.raw_data = None
        del train_X
        gc.collect()

        if self.cache_dir is not None:
            train_cache = os.path.join(self.cache_dir, 'train.bin')
            if not os.path.exists(self.cache_dir):
                os.makedirs(self.cache_dir)
            if os.path.exists(train_cache):
                os.remove(train_cache)
            dtrain.save_binary(train_cache)
            del dtrain
            gc.collect()
            dtrain = lgb.Dataset(train_cache, feature_name=feature_names)

        return dtrain

    def _prepare_valid_data(self, valid_X, valid_y, reference):
        print("Loading valid data...")
        if callable(valid_X):
            valid_X = valid_X()
        if hasattr(valid_X, 'to_df'):
            valid_X = valid_X.to_df(**self.df_opts)
        if callable(valid_y):
            valid_y = valid_y()

        if self.preprocessor is not None:
            print("Preprocessing valid data...")
            valid_X = self.preprocessor.transform(valid_X)

        print("Building valid dataset...")
        dvalid = lgb.Dataset(valid_X, label=valid_y, reference=reference)
        dvalid.raw_data = None
        del valid_X
        gc.collect()

        if self.cache_dir is not None:
            valid_cache = os.path.join(self.cache_dir, 'valid.bin')
            if not os.path.exists(self.cache_dir):
                os.makedirs(self.cache_dir)
            if os.path.exists(valid_cache):
                os.remove(valid_cache)
            dvalid.save_binary(valid_cache)
            del dvalid
            gc.collect()
            dvalid = lgb.Dataset(valid_cache, reference=reference)

        return dvalid

    def _prepare_test_data(self, X):
        if callable(X):
            X = X()
        if hasattr(X, 'to_df'):
            X = X.to_df(**self.df_opts)
        if self.preprocessor is not None:
            X = self.preprocessor.transform(X)
        return X
