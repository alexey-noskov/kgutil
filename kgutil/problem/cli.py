import argparse


class ProblemCli:

    def __init__(self, problem):
        self.problem = problem

    def run(self):
        commands = [c for c in self.problem.commands if c.available(self.problem)]

        parser = argparse.ArgumentParser()

        subparsers = parser.add_subparsers(title='commands', help='help', dest='command')
        subparsers.required = True
        subparsers.choices = [c.name for c in commands]

        for cmd in commands:
            self.add_command_parser(subparsers, cmd)

        args = parser.parse_args()

        command_args = {}
        command_args.update(vars(args))
        del command_args['func']
        del command_args['command']
        args.func(**command_args)

    def add_command_parser(self, subparsers, command):
        parser = subparsers.add_parser(command.name, help=command.help)

        if hasattr(command, 'subcommands'):
            available_subcommands = [c for c in command.subcommands if c.available(self.problem)]

            command_subparsers = parser.add_subparsers(title='commands', dest='command')
            command_subparsers.required = True
            command_subparsers.choices = [c.name for c in available_subcommands]

            for cmd in available_subcommands:
                self.add_command_parser(command_subparsers, cmd)
        else:
            parser.set_defaults(func=lambda: command.execute(self.problem))
