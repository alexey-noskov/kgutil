from ..tasks import build
from kgutil.util import nested_struct


class Command:

    def available(self, problem):
        return True

    def execute(self, problem):
        raise NotImplementedError


class CommandGroup:

    def available(self, problem):
        return any(c.available(problem) for c in self.subcommands)


class DataStatusCommand(Command):
    name = 'status'
    help = 'List all data files and their status'

    def execute(self, problem):
        print("Problem data:")
        for d in nested_struct.iter_values(problem.data):
            print("%s: %s" % (d.path if hasattr(d, 'path') else repr(d), d.exists()))


class DataDownloadCommand(Command):
    name = 'download'
    help = 'Download all required data files'

    def execute(self, problem):
        build(problem.data)


class DataCommandGroup(CommandGroup):
    name = 'data'
    help = 'data-related commands'

    subcommands = [DataStatusCommand(), DataDownloadCommand()]
