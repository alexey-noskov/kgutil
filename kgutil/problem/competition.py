import os
import tempfile

from kgutil.util import load_pickle, save_pickle, compression
from kgutil.tasks import Task, FileTarget


class KaggleCompetition:

    META_KEYS = set(['name', 'files'])

    def __init__(self, name, files=None, recompress=None):
        self.name = name
        self.files = files
        self.recompress = recompress

    def configure(self, problem):
        data = []
        for file_name in (self.files or self._retrieve_meta(problem)['files']):
            data.append(DownloadKaggleFileTask(
                problem,
                competition=self.name,
                directory=os.path.join(problem.config.data_dir, 'input'),
                file_name=file_name,
                recompress=self.recompress
            ).outputs)
        problem.add_data(self.name, data)

    def _retrieve_meta(self, problem):
        meta_file = os.path.join(problem.config.cache_dir, 'competition.pickle')

        # Try to load meta from cache
        if os.path.exists(meta_file):
            meta = load_pickle(meta_file)
            if set(meta.keys()) == self.META_KEYS and meta['name'] == self.name:
                return meta

        import kaggle
        meta = dict(
            name=self.name,
            files=[f.name for f in kaggle.api.competitionListFiles(self.name)]
        )
        os.makedirs(problem.config.cache_dir)
        save_pickle(meta_file, meta)
        return meta


class DownloadKaggleFileTask(Task):

    def __init__(self, problem, competition, directory, file_name, recompress):
        self.problem = problem
        self.competition = competition
        self.directory = directory
        self.file_name = file_name
        self.recompress = recompress

        # Determine file name after recompression
        if recompress is not None:
            self.out_file_name = file_name.rsplit('.', 1)[0] + '.' + recompress
        else:
            self.out_file_name = file_name

        self.inputs = []
        self.outputs = FileTarget(self, os.path.join(self.directory, self.out_file_name))

    def execute(self, _no_inputs):
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        import kaggle
        with tempfile.TemporaryDirectory(dir=self.directory, prefix='.download-') as tmp_dir:
            # Download file into temporary directory
            kaggle.api.competitionDownloadFile(self.competition, self.file_name, tmp_dir)

            # Recompress if required
            if self.recompress:
                compression.recompress_file(os.path.join(tmp_dir, self.file_name), os.path.join(tmp_dir, self.out_file_name))

            # Move to final place
            os.rename(os.path.join(tmp_dir, self.out_file_name), self.outputs.path)
        return self.outputs.load()
