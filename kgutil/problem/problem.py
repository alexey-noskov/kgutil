__all__ = ['Problem', 'get_default_problem', 'reset_default_problem']

from attrdict import AttrDict

from .competition import KaggleCompetition
from .command import DataCommandGroup
from .cli import ProblemCli


class Problem:

    default_config = dict(
        data_dir='data',
        cache_dir='cache',
    )

    def __init__(self):
        self.__default_problem_stack = []
        self.config = AttrDict(self.default_config)
        self.competition = None
        self.data = {}
        self.commands = []

        self.add_command(DataCommandGroup())

    def __enter__(self):
        global _default_problem
        self.__default_problem_stack.append(_default_problem)
        _default_problem = self
        return self

    def __exit__(self, *_exc_info):
        global _default_problem
        _default_problem = self.__default_problem_stack.pop()

    def run_cli(self):
        ProblemCli(self).run()

    def setup_competition(self, name, **kwargs):
        if self.competition is not None:
            raise RuntimeError("Competition already set")
        self.competition = KaggleCompetition(name, **kwargs)
        self.competition.configure(self)

    def add_data(self, name, data):
        self.data[name] = data

    def add_command(self, command):
        self.commands.append(command)


_default_problem = Problem()
_name_counter = 0


def get_default_problem():
    return _default_problem


def reset_default_problem():
    global _default_problem
    _default_problem = Problem()
