import os

from kgutil.util import nested_struct


class Task:

    def execute(self, inputs):
        raise NotImplementedError


class Target:

    def __init__(self, task):
        self.task = task

    def load(self):
        return None

    def exists(self):
        return False


class File:

    def __init__(self, path):
        self.path = path

    def __repr__(self):
        return "{}({})".format(type(self).__name__, self.path)


class FileTarget(Target):

    result_type = File

    def __init__(self, task, path):
        super().__init__(task)
        self.path = path

    def __repr__(self):
        return "{}({})".format(type(self).__name__, self.path)

    def load(self):
        return File(self.path)

    def exists(self):
        return os.path.exists(self.path)


def build(targets):
    cache = {}  # Task results and artifacts

    def build(obj):
        if isinstance(obj, Task):
            # Compute all inputs
            inputs = nested_struct.map_values(build, obj.inputs)

            print("Executing task %r" % obj)
            results = obj.execute(inputs)

            for tgt, res in zip(nested_struct.iter_values(obj.outputs), nested_struct.iter_values(results)):
                assert isinstance(res, tgt.result_type)
                cache[tgt] = res

            return results
        elif isinstance(obj, Target):
            res = cache.get(obj)
            if res is not None:
                return res

            # Try to load object from some external representation
            if obj.exists():
                res = obj.load()
            else:
                # Compute object by executing task
                res = dict(zip(nested_struct.iter_values(obj.task.outputs), nested_struct.iter_values(build(obj.task))))[obj]

            assert isinstance(res, obj.result_type)

            # Store object into cache
            cache[obj] = res
            return res
        else:
            raise ValueError("Unknown object: %r" % obj)

    return nested_struct.map_values(build, targets)
