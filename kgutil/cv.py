import itertools


class LimitFolds(object):

    def __init__(self, cv, n_splits):
        self.cv = cv
        self.n_splits = min(n_splits, cv.n_splits)

    def split(self, *args, **kwargs):
        return itertools.islice(self.cv.split(*args, **kwargs), self.n_splits)
