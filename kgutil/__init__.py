from .configuration import Config

config = Config()


config.update_default(
    debug_dir='debug',
    cache_dir='cache',

    callbacks=dict(
        registration=None
    ),

    models=dict(
        subdir='models',
        seed=12345,
        cv=None,
        cv_groups=lambda x: None,
        cv_deps=[],
        score=None,
        target=None,
        postprocess=None,
        fold_avg=False,
    ),

    features=dict(
        subdir='features',
    ),
)


def main():
    config.update(aaa=dict(bbb=3, ccc=4))

    print("Glob %s %s" % (config.aaa.bbb, config.aaa.ccc))

    with config.scoped(aaa=dict(bbb=5)):
        print("Scop %s %s" % (config.aaa.bbb, config.aaa.ccc))
