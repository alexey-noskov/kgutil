__all__ = ['Graph', 'Artifact', 'Task', 'get_default_graph', 'reset_default_graph']

from .name import Name

from kgutil.util import nested_struct


class Graph(object):

    def __init__(self):
        self.__default_graph_stack = []
        self.tasks = {}
        self.cache = {}

    def __enter__(self):
        global _default_graph
        self.__default_graph_stack.append(_default_graph)
        _default_graph = self
        return self

    def __exit__(self, *_exc_info):
        global _default_graph
        _default_graph = self.__default_graph_stack.pop()

    def add_task(self, task):
        return self.tasks.setdefault(str(task.name), task)

    def execute(self, obj):
        if id(obj) in self.cache:
            return self.cache[id(obj)]

        if isinstance(obj, Artifact):
            res = self.execute(obj.task)[obj.output_name]
        elif isinstance(obj, Task):
            res = obj.execute(nested_struct.map_values(self.execute, obj.inputs))
        else:
            raise ValueError("Invalid argument")

        self.cache[id(obj)] = res
        return res


class Task(object):

    @classmethod
    def create(cls, *args, **kwargs):
        return get_default_graph().add_task(cls(*args, **kwargs))

    def __init__(self, name):
        """
        name as a list of parts
        inputs as a nested dict structure
        outputs as a nested dict structure
        """

        self.name = Name(name)
        self.inputs = {}
        self.outputs = {}

    def iter_inputs(self):
        return nested_struct.iter_values(self.inputs)

    def iter_outputs(self):
        return nested_struct.iter_values(self.outputs)

    def execute(self, inputs):
        raise NotImplementedError


class Artifact(object):

    def __init__(self, task, output_name):
        self.task = task
        self.output_name = output_name
        self.name = Name(task.name + (output_name, ))

    def __repr__(self):
        return "Artifact(%s)" % str(self.name)

    def exists(self):
        raise NotImplementedError


_default_graph = Graph()
_name_counter = 0


def get_default_graph():
    return _default_graph


def reset_default_graph():
    global _default_graph
    _default_graph = Graph()
