from .graph import *
from .name import *


def execute(obj):
    return get_default_graph().execute(obj)
