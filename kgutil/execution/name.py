__all__ = ['Name']


class Name(tuple):

    def __new__(cls, parts):
        return tuple.__new__(cls, parts)

    def __str__(self):
        return '/'.join(self)

    def __setattr__(self, *ignored):
        raise NotImplementedError

    def __delattr__(self, *ignored):
        raise NotImplementedError
