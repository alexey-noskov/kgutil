from .workflow import Task, get_workflow


def task(outputs, inputs):
    def wrap(fn):
        return get_workflow().add_task(Task(outputs=outputs, inputs=inputs, fn=fn, name=fn.__name__))
    return wrap
