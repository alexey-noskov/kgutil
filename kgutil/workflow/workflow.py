from pathlib import Path

__all__ = ['Workflow', 'Artifact', 'Task', 'get_workflow', 'reset_workflow']


class Workflow:
    def __init__(self):
        self.artifacts = {}
        self.artifact_tasks = {}
        self.tasks = []

    def add_artifact(self, artifact):
        pass

    def add_task(self, task):
        for a in task.inputs:
            key = str(a)
            if key not in self.artifacts:
                self.artifacts[key] = Artifact(key)

        for a in task.outputs:
            key = str(a)
            if key not in self.artifacts:
                self.artifacts[key] = Artifact(key)
            if key in self.artifact_tasks:
                raise RuntimeError("Artifact %r already produced by other task" % key)
            self.artifact_tasks[key] = task

        self.tasks.append(task)

    def make(self, *artifacts):
        for a in artifacts:
            artifact_key = str(a)
            artifact = self.artifacts.get(artifact_key)
            if artifact is None:
                print(self.artifacts)
                raise RuntimeError("Unknown artifact %r" % artifact_key)
            elif artifact.exists():
                continue

            task = self.artifact_tasks.get(artifact_key)
            if task is None:
                raise RuntimeError("No task to make %r" % artifact_key)

            self.execute(task)

    def execute(self, task):
        # TODO Check for cyclic dependencies
        self.make(*task.inputs)

        print("Executing %s" % task.name)
        task_args = []
        for a in task.outputs:
            artifact = self.artifacts[str(a)]
            task_args.append(artifact)
            if not artifact.path.parent.exists():
                artifact.path.parent.mkdir(parents=True)
        for a in task.inputs:
            task_args.append(self.artifacts[str(a)])

        task.fn(*task_args)

    def __enter__(self):
        global _workflow
        self.__workflow_stack.append(_workflow)
        _workflow = self
        return self

    def __exit__(self, *_exc_info):
        global _workflow
        _workflow = self.__workflow_stack.pop()


class Artifact:
    def __init__(self, path):
        self.path = Path(path)

    def exists(self):
        return self.path.exists()


class Task:
    def __init__(self, outputs, inputs, fn, name):
        self.outputs = outputs
        self.inputs = inputs
        self.fn = fn
        self.name = name


_workflow = Workflow()


def get_workflow():
    return _workflow


def reset_workflow():
    global _workflow
    _workflow = Workflow()
