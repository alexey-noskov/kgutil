import numpy as np
import pandas as pd
import scipy.sparse as sp

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import KFold

from copy import deepcopy


default_cv = KFold(5, shuffle=True, random_state=12345)


class CvEncoder(BaseEstimator, TransformerMixin):

    def __init__(self, encoder, cv=default_cv):
        self.cv = cv
        self.encoder = encoder

    def fit_transform(self, X, y):
        preds = None
        for fold_train_idx, fold_pred_idx in self.cv.split(range(len(y)), y):
            fold_train_x = X.iloc[fold_train_idx]
            fold_pred_x = X.iloc[fold_pred_idx]

            fold_train_y = y.iloc[fold_train_idx]

            fold_model = deepcopy(self.encoder).fit(fold_train_x, fold_train_y)
            fold_pred = self.__predict(fold_model, fold_pred_x)

            if preds is None:
                preds = np.ones((len(y), fold_pred.shape[1]) if len(fold_pred.shape) > 1 else len(y)) * np.nan

            if sp.issparse(fold_pred):
                fold_pred = fold_pred.todense()

            preds[fold_pred_idx] = fold_pred

        # Fit encoder on train
        self.encoder.fit(X, y)

        return preds

    def transform(self, X):
        p = self.__predict(self.encoder, X)

        if sp.issparse(p):
            p = p.todense()

        return p

    def __predict(self, encoder, x):
        if hasattr(encoder, 'predict_proba'):
            return encoder.predict_proba(x)

        if hasattr(encoder, 'predict'):
            return encoder.predict(x)

        return encoder.transform(x)


class CvMeanEncoder(BaseEstimator, TransformerMixin):

    def __init__(self, smooth=10, cv=default_cv):
        self.smooth = smooth
        self.cv = cv

    def fit_transform(self, x, y):
        x = pd.Series(x)
        y = pd.Series(y)

        self.global_mean = y.mean()
        self.value_means = (y.groupby(x).sum() + self.global_mean * self.smooth) / (y.groupby(x).count() + self.smooth)

        res = pd.Series(np.nan, index=y.index)

        for train_idx, pred_idx in self.cv.split(range(len(y))):
            train_x = x.iloc[train_idx]
            pred_x = x.iloc[pred_idx]

            train_y = y.iloc[train_idx]

            global_mean = train_y.mean()
            value_means = (train_y.groupby(train_x).sum() + global_mean * self.smooth) / (train_y.groupby(train_x).count() + self.smooth)

            res.loc[y.index[pred_idx]] = self.fillna_if_smooth(pred_x.map(value_means), global_mean)

        return res.values

    def transform(self, x):
        return self.fillna_if_smooth(x.map(self.value_means), self.global_mean).values

    def fillna_if_smooth(self, df, value):
        return df.fillna(value) if self.smooth > 0 else df


class StatsEncoder(BaseEstimator, TransformerMixin):

    def __init__(self, stats='mean', smooth=10):
        self.smooth = smooth
        self.stats = stats

    def fit(self, x, y):
        x = pd.Series(x)
        y = pd.Series(y)

        a = y.groupby(x).agg(self.stats)
        c = y.groupby(x).count()

        self.global_stats = y.agg(self.stats)
        self.value_stats = (a * c + self.global_stats * self.smooth) / (c + self.smooth)

        return self

    def transform(self, x):
        return self.fillna_if_smooth(x.map(self.value_stats), self.global_stats).values

    def fillna_if_smooth(self, df, value):
        return df.fillna(value) if self.smooth > 0 else df
