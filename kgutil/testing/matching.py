
class Any(object):

    def matches(self, x):
        return True


def matches(obj, spec):
    return all(m.matches(x) if hasattr(m, 'matches') else m == x for m, x in zip(spec, obj))
