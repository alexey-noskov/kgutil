import os

from .. import config
from .core import Node
from ..data import NamedArray

from copy import deepcopy


class FnNode(Node):

    def __init__(self, name, deps, fn, extract_deps=False, opts={}):
        super(FnNode, self).__init__(name, deps, path=os.path.join(config.features.subdir, name))
        self.fn = fn
        self.extract_deps = extract_deps
        self.opts = opts

    def compute(self, *dss):
        if self.extract_deps:
            return [self.fn(*[ds[d] for d in self.dependencies], **self.opts) for ds in dss]
        else:
            return [self.fn(ds, **self.opts) for ds in dss]


class FeatureExtractorNode(Node):

    def __init__(self, name, extractor, opts={}):
        super(FeatureExtractorNode, self).__init__(name, extractor.deps, path=os.path.join(config.features.subdir, name))
        self.extractor = extractor
        self.local = True
        self.cache = None
        self.opts = opts

    def compute(self, train_ds, *test_dss):
        extractor = deepcopy(self.extractor)
        if len(self.opts) > 0:
            extractor.set_params(**self.opts)

        results = []
        results.append(extractor.fit_transform(train_ds.name, train_ds))

        for test_ds in test_dss:
            results.append(extractor.transform(test_ds.name, test_ds))

        return results


class SklearnFeaturePipeline(object):

    def __init__(self, name, dep, columns, target, *transformers):
        self.name = name
        self.deps = [dep, target]
        self.columns = columns
        self.target = target
        self.transformers = transformers

    def fit_transform(self, dataset_name, deps):
        res = deps[self.deps[0]]
        trg = deps[self.deps[1]]

        if self.columns is not None:
            res = res[self.columns]

        for tr in self.transformers:
            res = tr.fit_transform(res, trg)

        if len(res.shape) == 1:
            res = res.reshape(-1, 1)

        return NamedArray(res, ['c%d' % i for i in xrange(res.shape[1])])

    def transform(self, dataset_name, deps):
        res = deps[self.deps[0]]

        if self.columns is not None:
            res = res[self.columns]

        for tr in self.transformers:
            res = tr.transform(res)

        if len(res.shape) == 1:
            res = res.reshape(-1, 1)

        return NamedArray(res, ['c%d' % i for i in xrange(res.shape[1])])
