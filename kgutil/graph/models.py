from __future__ import print_function

import numpy as np

import os

from .core import Node, resolve, compute_node
from ..data import Dataset
from .. import config

from copy import deepcopy

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import FunctionTransformer


def build_processing_pipeline(procs):
    if not isinstance(procs, list):
        procs = [procs]

    steps = []
    for pr in procs:
        if hasattr(pr, 'fit_transform') and hasattr(pr, 'transform'):
            steps.append(deepcopy(pr))
        elif callable(pr):
            steps.append(FunctionTransformer(pr, validate=False))
        else:
            raise ValueError("Unknown processor %s" % type(pr))

    return Pipeline([('s%d' % i, s) for i, s in enumerate(steps)])


def build_deps(global_deps, fold_deps):
    deps = list(global_deps)

    def add_dep(n):
        if n.local:
            for d in n.dependencies:
                add_dep(resolve(d))
        elif n.name not in deps:
            deps.append(n.name)

    for d in fold_deps:
        add_dep(resolve(d))

    return deps


def aggregate_scores(scores):
    res = {}
    for k in scores[0].keys():
        values = [s[k] for s in scores]
        res[k] = [np.mean(values), np.std(values)]
    return res


class ModelNode(Node):

    def __init__(self, name, deps, **opts):
        super(ModelNode, self).__init__(name, [opts.get('target', config.models.target)] + deps, path=os.path.join(config.models.subdir, name))
        self.options = config.models + opts

    def compute(self, *datasets):
        return self.apply_model(*datasets)


class BasicModelNode(ModelNode):

    def __init__(self, name, model, global_features, fold_features, check_columns=True, **opts):
        super(BasicModelNode, self).__init__(name, build_deps(global_features + (config.models + opts).cv_deps, fold_features), **opts)

        self.global_features = global_features
        self.fold_features = fold_features
        self.all_features = global_features + fold_features
        self.model = model
        self.check_columns = check_columns

    def apply_model(self, train_ds, test_ds):
        print("Training model %s..." % self.name)

        fold_dependencies = [resolve(f) for f in self.fold_features]

        train_y = train_ds[self.options.target]
        train_p = None

        fold_models = []

        if self.options.cv is not None:
            eval_scores = [None] * self.options.cv.n_splits

            for fold_index, (fold_train_index, fold_test_index) in enumerate(self.options.cv.split(range(len(train_y)), train_y, groups=self.options.cv_groups(train_ds))):
                print("  Fold %d:" % fold_index)

                # Collect per-fold datasets
                fold_train_ds = train_ds.subset('%s-ft%d' % (train_ds.name, fold_index), fold_train_index)
                fold_test_ds = train_ds.subset('%s-fe%d' % (train_ds.name, fold_index), fold_test_index)

                # Apply model
                fold_model, fold_test_p = self.apply_model_fold(fold_train_ds, fold_test_ds, fold_dependencies=fold_dependencies, context=self.context.child('fold-%d' % fold_index))
                fold_test_y = train_y.iloc[fold_test_index]
                fold_scores = self.options.score(fold_test_y, fold_test_p)

                if not isinstance(fold_scores, dict):
                    fold_scores = {'score': fold_scores}

                # Now we know predictions dim and may allocate array for it
                if train_p is None:
                    train_p = np.ones((len(train_y), fold_test_p.shape[1]) if len(fold_test_p.shape) > 1 else len(train_y)) * np.nan

                train_p[fold_test_index] = fold_test_p
                eval_scores[fold_index] = fold_scores

                for k, v in fold_scores.items():
                    print("    %s: %.6f" % (k, v))

                del fold_train_ds, fold_test_ds

                if self.options.fold_avg:
                    fold_models.append(fold_model)

            eval_scores_agg = aggregate_scores(eval_scores)
            for k, (mean, std) in eval_scores_agg.items():
                print("  CV %s: %.6f +- %.6f" % (k, mean, std))

            if self.context.debug_dir is not None:
                if not os.path.exists(self.context.debug_dir):
                    os.makedirs(self.context.debug_dir)

                with open(os.path.join(self.context.debug_dir, 'cv.txt'), 'w') as out:
                    for i, s in enumerate(eval_scores):
                        for k, v in s.items():
                            out.write('fold %d %s: %f\n' % (i, k, v))
                    for k, (mean, std) in eval_scores_agg.items():
                        out.write('cv %s mean: %f, std: %f\n' % (k, mean, std))

        # Full train
        print("  Full...")

        if self.options.fold_avg:
            test_p = self._apply_fold_avg(train_ds.copy(), test_ds.copy(), fold_dependencies=fold_dependencies, fold_models=fold_models)

        else:
            _, test_p = self.apply_model_fold(train_ds.copy(), test_ds.copy(), fold_dependencies=fold_dependencies, context=self.context.child('full'))

        # Postprocessing
        if self.options.postprocess is not None:
            pipeline = build_processing_pipeline(self.options.postprocess)

            if train_p is not None:
                train_p = pipeline.fit_transform(train_p)
            test_p = pipeline.transform(test_p)

        print("  Done.")

        return train_p, test_p

    def apply_model_fold(self, train_ds, test_ds, fold_dependencies, context):
        train_X, train_y, test_X, test_y = self._prepare_fold_data(train_ds, test_ds, fold_dependencies=fold_dependencies)

        print("    Training on %d x %d..." % train_X.shape)

        model = deepcopy(self.model)

        # Apply model
        return model, model.fit_predict(train_X, train_y, test_X, test_y, seed=self.options.seed, context=context)

    def _apply_fold_avg(self, train_ds, test_ds, fold_dependencies, fold_models):
        assert len(fold_models) > 0

        _, _, test_X, _ = self._prepare_fold_data(train_ds, test_ds, fold_dependencies=fold_dependencies)

        test_p = None
        for fold_model in fold_models:
            fold_p = fold_model.predict(test_X)

            if test_p is None:
                test_p = fold_p
            else:
                test_p += fold_p

        test_p /= len(fold_models)

        return test_p

    def _prepare_fold_data(self, train_ds, test_ds, fold_dependencies):
        print("    Building features...")

        # Apply fold feature extractors
        for node in fold_dependencies:
            train_ds[node.name], test_ds[node.name] = compute_node(node, [train_ds, test_ds])

        train_X = train_ds.combine_slices(self.all_features)
        test_X = test_ds.combine_slices(self.all_features)

        train_y = train_ds[self.options.target]
        test_y = test_ds[self.options.target]

        if self.check_columns and list(train_X.columns) != list(test_X.columns):
            raise ValueError("Train and test columns differ: %s != %s" % (train_X.columns, test_X.columns))

        return train_X, train_y, test_X, test_y
