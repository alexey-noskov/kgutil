import numpy as np
import pandas as pd

from ..data import Dataset, NamedArray
from .core import Node


def concat_datasets(datasets, slice_names=None):
    name = '+'.join(ds.name for ds in datasets)
    slices = {}

    if slice_names is None:
        slice_names = datasets[0].slices.keys()

    for key in slice_names:
        if isinstance(datasets[0].slices[key], (pd.DataFrame, pd.Series)):
            slices[key] = pd.concat([ds.slices[key] for ds in datasets], axis=0)
        elif isinstance(datasets[0].slices[key], NamedArray):
            slices[key] = NamedArray(np.concatenate([ds.slices[key].values for ds in datasets]), columns=datasets[0].slices[key].columns)
        else:
            slices[key] = np.concatenate([ds.slices[key] for ds in datasets])

    return Dataset(name, slices)


class JointNode(Node):

    def __init__(self, node):
        super(JointNode, self).__init__(node.name, node.dependencies, path=node.context.path)
        self.node = node

    def compute(self, *datasets):
        offsets = [0] + list(np.cumsum([len(ds) for ds in datasets]))

        joint_dataset = concat_datasets(datasets, slice_names=self.dependencies)
        joint_result = self.node.compute(joint_dataset)[0]

        results = []
        for i in xrange(len(datasets)):
            results.append(joint_result.iloc[offsets[i]:offsets[i+1]])

        return results
