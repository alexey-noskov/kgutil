from .core import register
from .joint import JointNode
from .features import FeatureExtractorNode, FnNode
from .models import BasicModelNode


def register_model(name, model_fn, global_features, fold_features=[], **opts):
    register(BasicModelNode(name, model_fn, global_features, fold_features, **opts))


def register_features(*args, **kwargs):
    deps = kwargs.get('deps', [])
    joint = kwargs.get('joint', False)
    extract_deps = kwargs.get('extract_deps', True)
    name = kwargs.get('name', None)
    opts = kwargs.get('opts', {})

    def decorator(fn):
        if callable(fn):
            node = FnNode(name or fn.__name__, deps, fn, extract_deps=extract_deps, opts=opts)
        else:
            node = FeatureExtractorNode(name or fn.name, fn, opts=opts)

        if joint:
            node = JointNode(node)

        register(node)

        return fn

    if len(args) > 0:
        assert len(args) == 1
        return decorator(args[0])
    else:
        return decorator
