from ..data import Dataset
from ..util import save_pickle, load_pickle
from .. import config

import os


class Context(object):

    def __init__(self, path):
        self.path = path
        self.debug_dir = os.path.join(config.debug_dir, path)
        self.cache_dir = os.path.join(config.cache_dir, path)

    def child(self, path):
        return Context(os.path.join(self.path, path))


class Cache(object):

    def __init__(self, context):
        self.cache_dir = context.cache_dir

    def read(self, dataset_names):
        cache_file_names = list(map(self.get_cache_file_name, dataset_names))

        if all(map(os.path.exists, cache_file_names)):
            return list(map(load_pickle, cache_file_names))

    def write(self, dataset_names, results):
        if not os.path.exists(self.cache_dir):
            os.makedirs(self.cache_dir)

        for ds_name, res in zip(dataset_names, results):
            save_pickle(self.get_cache_file_name(ds_name), res)

    def get_cache_file_name(self, ds_name):
        return os.path.join(self.cache_dir, '%s.pickle' % ds_name)


class Node(object):

    def __init__(self, name, dependencies, cached=True, path=None):
        self.name = name
        self.dependencies = dependencies
        self.context = Context(path or name)
        self.local = any(resolve(d).local for d in self.dependencies)
        self.cache = Cache(self.context) if cached and not self.local else None

    def compute(self, *datasets):
        raise NotImplemented


registered_nodes = {}


def resolve(name):
    return registered_nodes[name]


def register(n):
    registered_nodes[n.name] = n

    if config.callbacks.registration is not None:
        cbs = config.callbacks.registration

        if callable(cbs):
            cbs = [cbs]

        for cb in cbs:
            cb(n)


def resolve_with_dependencies(*names, **opts):
    ignored = set(opts.get('ignore', []))

    return [resolve(n) for n in names if n not in ignored]


def compute_node(node, datasets, validator=None):
    dataset_names = [ds.name for ds in datasets]

    # Try to load node cache if present
    if node.cache is not None:
        cache = node.cache.read(dataset_names)

        # Check if node results cached
        if cache is not None:
            return cache

    # Compute all dependencies
    for dep_name in node.dependencies:
        if all(dep_name in ds.slices for ds in datasets):
            continue

        dep = resolve(dep_name)

        if validator:
            validator(dep)

        results = compute_node(dep, datasets)

        for ds, res in zip(datasets, results):
            ds[dep.name] = res

    print("Computing node %s..." % node.name)

    # Compute node itself
    results = node.compute(*datasets)

    # Save node cache
    if node.cache is not None:
        node.cache.write(dataset_names, results)

    return results


def compute(node_name, ds_names, validator=None):
    datasets = [Dataset(name, {}) for name in ds_names]
    node = resolve(node_name)

    return compute_node(node, datasets, validator)
