__all__ = ['Data']

from .core import Node
from ..execution import Task, Artifact


class Data(Node):

    def __init__(self, reader, name=None):
        super(Data, self).__init__(name)
        self.impl = reader

    def read(self, state, part):
        assert state is None
        return None, ReadTask(self, part).outputs['result']


class ReadTask(Task):

    def __init__(self, data, part):
        super(ReadTask, self).__init__(name=[data.name, 'read'])
        self.data = data
        self.part = part
        self.inputs = {}
        self.outputs = {'result': Artifact(self, 'result')}

    def execute(self, _inputs):
        return {
            'result': self.data.impl.read(self.part)
        }
