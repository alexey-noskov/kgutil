from .core import *
from .data import *
from .transformer import *

from kgutil import execution


class StatefulApi(object):

    def __init__(self, node, methods):
        self.node = node
        self.methods = methods
        self.state = None

    def __getattr__(self, attr):
        if attr in self.methods:
            def fn(*a, **kwa):
                return self.call(attr, *a, **kwa)
            return fn
        else:
            raise AttributeError("%r object has no attribute %r" % (self.__class__, attr))

    def call(self, method, *args, **kwargs):
        self.state, res = getattr(self.node, method)(self.state, *args, **kwargs)
        return execution.execute(res)
