__all__ = ['Transformer']

from .core import Node, NodeState
from ..execution import Task, Artifact

from kgutil.util import nested_struct, call_with
from copy import copy, deepcopy

from sklearn.base import BaseEstimator, TransformerMixin


class FunctionTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, function):
        self.function = function

    def fit(self, *args, **kwargs):
        return self

    def fit_transform(self, *args, **kwargs):
        return self.function(*args, **kwargs)

    def transform(self, *args, **kwargs):
        return self.function(*args, **kwargs)


class Transformer(Node):

    def __init__(self, transformer, inputs, name=None):
        super(Transformer, self).__init__(
            name=name,
            dependencies=inputs
        )

        if not isinstance(inputs, (list, tuple, dict)):
            inputs = [inputs]

        self.impl = prepare_transformer_impl(transformer)
        self.inputs = inputs
        self.fit_inputs = []

        self.prefit_data_part = None

    def fit_on(self, data_part):
        n = copy(self)
        n.prefit_data_part = data_part
        return n

    def fit_with(self, inputs):
        if not isinstance(inputs, (list, tuple, dict)):
            inputs = [inputs]

        n = copy(self)
        n.fit_inputs = inputs
        return n

    def fit_transform(self, state, data_part):
        if state is None:
            state = self.__make_empty_state()

        # Transform inputs first
        inputs, input_states = self.__transform_deps('fit_transform', data_part, self.inputs, state.dependencies['inputs'])
        fit_inputs, fit_input_states = self.__transform_deps('fit_transform', data_part, self.fit_inputs, state.dependencies['fit_inputs'])

        task = FitTransformTask.create(self, state.artifact, data_part, inputs, fit_inputs)

        return NodeState(task.outputs['state'], {'inputs': input_states, 'fit_inputs': fit_input_states}), task.outputs['result']

    def transform(self, state, data_part):
        if state is None:
            state = self.__make_empty_state()

        # Transform inputs first
        inputs, input_states = self.__transform_deps('transform', data_part, self.inputs, state.dependencies['inputs'])

        task = TransformTask.create(self, state.artifact, data_part, inputs)

        return NodeState(state.artifact, {'inputs': input_states, 'fit_inputs': state.dependencies['fit_inputs']}), task.outputs['result']

    def __make_empty_state(self):
        state = NodeState(None, {
            'inputs': nested_struct.map_values(lambda x: None, self.inputs),
            'fit_inputs': nested_struct.map_values(lambda x: None, self.fit_inputs)
        })

        if self.prefit_data_part:
            inputs, input_states = self.__transform_deps('fit_transform', self.prefit_data_part, self.inputs, state.dependencies['inputs'])
            fit_inputs, fit_input_states = self.__transform_deps('fit_transform', self.prefit_data_part, self.fit_inputs, state.dependencies['fit_inputs'])

            task = FitTransformTask.create(self, state.artifact, self.prefit_data_part, inputs, fit_inputs)
            state = NodeState(task.outputs['state'], {'inputs': input_states, 'fit_inputs': fit_input_states})

        return state

    def __transform_deps(self, method, data_part, deps, dep_states):
        def transform_dep(dep, state):
            if hasattr(dep, method):
                new_dep_state, dep_out = getattr(dep, method)(state, data_part)
            elif hasattr(dep, 'read'):
                new_dep_state, dep_out = dep.read(state, data_part)
            else:
                raise RuntimeError("Unknown dependency: %s" % dep)
            return new_dep_state, dep_out

        dep_results = nested_struct.map_values(transform_dep, deps, dep_states)

        dep_outs = nested_struct.map_values(lambda _d, t: t[1], deps, dep_results)
        new_dep_states = nested_struct.map_values(lambda _d, t: t[0], deps, dep_results)

        return dep_outs, new_dep_states


class FitTransformTask(Task):

    def __init__(self, transformer, state, data_part, inputs, fit_inputs):
        super(FitTransformTask, self).__init__(name=[transformer.name, 'fit_transform', data_part])  # TODO Hash by state
        self.transformer = transformer
        self.state = state
        self.inputs = nested_struct.drop_empty_values({
            'state': state,
            'inputs': inputs,
            'fit_inputs': fit_inputs
        })
        self.outputs = {
            'state': Artifact(self, 'state'),
            'result': Artifact(self, 'result')
        }

    def execute(self, inputs):
        state = deepcopy(inputs.get('state', self.transformer.impl))
        result = call_with(state.fit_transform, inputs['inputs'], inputs['fit_inputs'])

        return {
            'state': state,
            'result': result
        }


class TransformTask(Task):

    def __init__(self, transformer, state, data_part, inputs):
        super(TransformTask, self).__init__(name=[transformer.name, 'transform', data_part])  # TODO Hash artifacts by state
        self.transformer = transformer
        self.inputs = nested_struct.drop_empty_values({
            'state': state,
            'inputs': inputs
        })
        self.outputs = {
            'result': Artifact(self, 'result')
        }

    def execute(self, inputs):
        state = inputs.get('state', self.transformer.impl)  # TODO Check what call immutable ?

        return {
            'result': call_with(state.transform, inputs['inputs'])
        }


def prepare_transformer_impl(transformer):
    if hasattr(transformer, 'transform'):
        return transformer
    elif callable(transformer):
        return FunctionTransformer(transformer)
    else:
        raise ValueError("Unknown transformer: %r" % transformer)
