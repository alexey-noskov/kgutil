__all__ = ['Graph', 'Node', 'get_default_graph', 'reset_default_graph']

from copy import copy


class Graph(object):

    def __init__(self):
        self.__default_graph_stack = []
        self.nodes = {}

    def __enter__(self):
        global _default_graph
        self.__default_graph_stack.append(_default_graph)
        _default_graph = self
        return self

    def __exit__(self, *_exc_info):
        global _default_graph
        _default_graph = self.__default_graph_stack.pop()

    def __setattr__(self, name, value):
        if isinstance(value, Node):
            self.add(value.with_name(name))
        else:
            super(Graph, self).__setattr__(name, value)

    def __getattr__(self, name):
        if name in self.nodes:
            return self.nodes[name]
        else:
            super(Graph, self).__getattr__(name)

    def add(self, node):
        if node.name is None:
            raise ValueError("Nameless nodes can't be added to graph")
        if node.name in self.nodes:
            raise ValueError("Node with such name already exists in graph")
        self.nodes[node.name] = node

    #def node(self, obj, name=None):


class Node(object):

    def __init__(self, name=None, dependencies=[]):
        self.name = name
        self.dependencies = dependencies

    def with_name(self, name):
        n = copy(self)
        n.name = name
        return n


class NodeState(object):
    def __init__(self, artifact, dependencies):
        self.artifact = artifact
        self.dependencies = dependencies

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return "NodeState(%5, %r)" % (self.artifact, self.dependencies)


_default_graph = Graph()
_name_counter = 0


def get_default_graph():
    return _default_graph


def reset_default_graph():
    global _default_graph
    _default_graph = Graph()


def generate_name(cls):
    global _name_counter
    _name_counter += 1
    return "%s-%d" % (cls.__name__, _name_counter)
