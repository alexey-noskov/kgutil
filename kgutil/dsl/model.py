__all__ = ['Model']

from .core import Node


class Model(Node):

    def fit(self, state, data_part):
        """ Return execution graph fitting model """

        dep_results = []
        for dep in self.dependencies:
            dep_res = dep.fit_transform(state, data_part)  # TODO Fallback to transform
            dep_results.append(dep_res)

        task = self.ModelFitTask(dep_results, ....)


    def predict(self, state, data_part):
        """ Return execution graph to predict with model """

        new_state = ModelState()
        dep_results = []
        for dep in self.dependencies:
            dep_state, dep_res = dep.transform(state.dep_states[dep], data_part)

            new_state.dep_states[dep] = dep_state
            dep_results.append(dep_res)

        task = self.ModelPredictTask(dep_results, ....)


class ModelState(object):

    def __init__(self):
        self.dep_states = {}


class ModelFitTask(object):

    def __init__(self, state, features, targets):
        self.inputs = [state] + features + targets
        self.outputs = 'new_state'

class ModelPredictTask(object):

    def __init__(self, state, features):
        pass
