__all__ = ['SelectIndexer', 'NamedArray', 'Dataset', 'to_df']

import pandas as pd
import numpy as np

from kgutil.util import hstack


class SelectIndexer(object):

    def __init__(self, obj):
        self.obj = obj

    def __getitem__(self, key):
        return self.obj.select(key)


class NamedArray(object):

    def __init__(self, values, columns):
        self.values = values
        self.columns = columns
        self.iloc = SelectIndexer(self)
        self.shape = self.values.shape

    def select(self, idx):
        return NamedArray(self.values[idx], self.columns)


class Dataset(object):
    """ Dataset which consists of several column slices """

    def __init__(self, name, slices):
        self.name = name
        self.slices = slices

    def __getitem__(self, key):
        return self.slices[key]

    def __setitem__(self, key, value):
        self.slices[key] = value

    def __len__(self):
        for s in self.slices.values():
            if s is not None:
                return s.shape[0]

        raise ValueError("There are no non-empty slice")

    def copy(self):
        return Dataset(self.name, dict(self.slices))

    def combine_slices(self, slice_names, dtype=np.float32):
        if all(isinstance(self.slices[sn], pd.DataFrame) for sn in slice_names):
            # Special case: dataframe concat to preserve dtypes
            def prepare_df(sn, df):
                columns = dict((cn, '%s__%s' % (sn, cn)) for cn in df.columns)
                return df.rename(columns=columns)

            return pd.concat([prepare_df(sn, self.slices[sn]) for sn in slice_names], axis=1)

        values = []
        columns = []

        for sn in slice_names:
            s = self.slices[sn]

            if hasattr(s, 'values'):
                sv = s.values
            else:
                sv = s

            if len(s.shape) == 1:
                sv = sv.reshape(-1, 1)  # Ensure slice values are 2d
                sc = [sn]
            elif hasattr(s, 'columns'):
                sc = ['%s__%s' % (sn, cn) for cn in s.columns]
            else:
                sc = ['%s__c%d' % (sn, ci) for ci in xrange(sv.shape[1])]

            values.append(sv)
            columns.extend(sc)

        return NamedArray(hstack(values, dtype=dtype), columns=columns)

    def subset(self, name, index):
        slices = {}
        for name, values in self.slices.items():
            if hasattr(values, 'iloc'):
                slices[name] = values.iloc[index]
            else:
                slices[name] = values[index]

        return Dataset(name, slices)


def to_df(df):
    if isinstance(df, pd.DataFrame):
        return df

    if hasattr(df, 'values') and hasattr(df, 'columns'):
        return pd.DataFrame(df.values, columns=df.columns)

    if len(df.shape) == 1:
        return pd.DataFrame({'col': df})

    if len(df.shape) == 2:
        return pd.DataFrame(df, columns=['c%d' % i for i in xrange(df.shape[1])])

    raise ValueError("Unknown object %s" % type(df))
