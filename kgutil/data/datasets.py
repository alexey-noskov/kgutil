import pandas as pd
import numpy as np

from copy import copy
from kgutil.util import timer


__all__ = ['Dataset', 'JoinedDataset']


class Dataset:
    """
    Lazy dataset object
    """

    @property
    def columns(self):
        """
        Return names of columns in dataset
        """
        raise NotImplementedError

    @property
    def dtypes(self):
        """
        Return column dtypes in dataset
        """
        raise NotImplementedError

    def to_df(self, dtype=None):
        """
        Read all data as Pandas dataframe, maybe converting to some dtype
        """
        raise NotImplementedError

    def to_array(self, dtype=None):
        """
        Read all data as Numpy array, maybe converting to dtype
        """
        return self.to_df(dtype).values

    def iter_df_batches(self, batch_size, dtype=None):
        """
        Iterate over batches in form of Pandas dataframes
        """
        raise NotImplementedError

    def iter_array_batches(self, batch_size, dtype=None):
        """
        Iterate over batches in form of Numpy arrays
        """
        raise NotImplementedError


class CsvDataset(Dataset):

    def __init__(self, filename, dtypes, column_indices, column_names, rows=None):
        self._filename = filename
        self._dtypes = dtypes
        self._column_indices = list(column_indices)
        self._column_names = list(column_names)
        self._rows = rows

    @property
    def columns(self):
        return copy(self._column_names)

    @property
    def dtypes(self):
        return copy(self._dtypes)

    def to_df(self, dtype=None):
        with timer("Loading %s rows from %s with columns %r" % (len(self._rows) if self._rows is not None else '', self._filename, self._column_names)):
            return self._read_df(dtype=None)

    def iter_df_batches(self, batch_size, dtype=None):
        assert batch_size is not None
        return self._read_df(batch_size=batch_size, dtype=dtype)

    def _read_df(self, batch_size=None, dtype=None):
        if dtype is None:
            dtype = self._dtypes

        idx = None
        if self._rows is None:
            nrows = None
            skiprows = 1
        elif isinstance(self._rows, range):
            assert self._rows.start >= 0
            nrows = self._rows.stop - self._rows.start
            skiprows = self._rows.start + 1
        else:
            rows = np.asarray(self._rows)
            min_row = rows.min()
            max_row = rows.max()
            assert min_row >= 0

            # If more then half rows present, load range and then take subset
            if batch_size is None and rows.shape[0] * 2 > max_row - min_row:
                skiprows = min_row + 1
                nrows = max_row - min_row + 1
                idx = rows - min_row
            else:
                nrows = rows.shape[0]
                rows = set(rows + 1)
                skiprows = lambda i: i not in rows

        res = pd.read_csv(
            self._filename,
            names=self._column_names, usecols=self._column_indices, index_col=False,
            dtype=dtype, engine='c',
            skiprows=skiprows, nrows=nrows, chunksize=batch_size)

        if batch_size is None:
            if nrows is not None:
                assert res.shape[0] == nrows

            if idx is not None:
                res = res.iloc[idx]
                res.reset_index(drop=True, inplace=True)

        return res


class JoinedDataset(Dataset):

    def __init__(self, parts, names=None, name_delimiter=None):
        assert names is None or len(names) == len(parts)

        self._parts = parts
        self._names = names
        self._name_delimiter = name_delimiter or '/'

        self._columns = None
        self._dtypes = None

    @property
    def columns(self):
        if self._columns is None:
            self._columns = self._build_columns()
        return copy(self._columns)

    @property
    def dtypes(self):
        if self._dtypes is None:
            self._dtypes = self._build_dtypes()
        return copy(self._dtypes)

    def to_df(self, dtype=None):
        return self._join_df([p.to_df(dtype) for p in self._parts])

    def iter_df_batches(self, batch_size, dtype=None):
        for batch in zip(*[p.iter_df_batches(batch_size, dtype) for p in self._parts]):
            yield self._join_df(batch)

    def _join_df(self, part_dfs):
        if self._names is not None:
            part_dfs = [df.add_prefix(n + self._name_delimiter) for df, n in zip(part_dfs, self._names)]
        return pd.concat(part_dfs, axis=1, copy=False)

    def _build_columns(self):
        if self._names is None:
            return sum([p.columns for p in self._parts], [])
        else:
            return [n + self._name_delimiter + c for p, n in zip(self._parts, self._names) for c in p.columns]

    def _build_dtypes(self):
        res = {}
        if self._names is None:
            for p in self._parts:
                for c, d in p.dtypes.items():
                    res[c] = d
        else:
            for p, n in zip(self._parts, self._names):
                for c, d in p.dtypes.items():
                    res[n + self._name_delimiter + c] = d
        return res
