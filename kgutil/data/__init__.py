import pandas as pd
import numpy as np
import fnmatch

from .datasets import CsvDataset, JoinedDataset


__all__ = ['csv_dataset', 'concat', 'iter_df_batches']


## Helper functions

def csv_dataset(filename, columns=None, rows=None, dtypes=None, sample_size=10000):
    sample = pd.read_csv(filename, dtype=dtypes, nrows=sample_size)

    dtypes = dict(sample.dtypes)
    column_indices = range(len(sample.columns))
    column_names = list(sample.columns)
    del sample

    if columns is not None:
        column_pairs = set()
        for col in columns:
            matched = False
            for i, c in enumerate(column_names):
                if fnmatch.fnmatch(c, col):
                    column_pairs.add((i, c))
                    matched = True
            if not matched:
                raise RuntimeError("No columns matched with %r (available: %r)" % (col, column_names))

        column_indices, column_names = zip(*sorted(column_pairs))

    return CsvDataset(
        filename,
        dtypes=dtypes, column_indices=column_indices, column_names=column_names,
        rows=rows
    )


def concat(datasets, axis, names=None, name_delimiter=None):
    if axis == 1:
        return JoinedDataset(datasets, names=names, name_delimiter=name_delimiter)
    else:
        raise RuntimeError("Concat on axis %d isn't supported" % axis)


def iter_df_batches(*datasets, **options):
    """
    Iterate over one or several datasets in batches represented by dataframes (or series)
    """
    batch_size = options.pop('batch_size')
    dtype = options.pop('dtype', None)
    if options:
        raise ValueError("Unexpected options: %r" % options.keys())

    def iter_pandas(df):
        batch_start = 0
        while batch_start < df.shape[0]:
            yield df.iloc[batch_start:batch_start+batch_size]
            batch_start += batch_size

    def iter_single(ds):
        if hasattr(ds, 'iter_df_batches'):
            return ds.iter_df_batches(batch_size, dtype=dtype)
        elif isinstance(ds, (pd.DataFrame, pd.Series)):
            return iter_pandas(ds)
        elif isinstance(ds, (list, np.ndarray)):
            return iter_pandas(pd.Series(ds))
        else:
            raise RuntimeError("Unsupported dataset %r" % ds)

    if len(datasets) == 1:
        return iter_single(datasets[0])
    else:
        return zip(*map(iter_single, datasets))


class DataReader(object):

    parts = NotImplementedError

    #schema = NotImplementedError  # No schema control for now

    def read(self, part):
        raise NotImplementedError


class ConstDataReader(DataReader):

    def __init__(self, **parts):
        self.parts = parts.keys()
        self.contents = parts

    def read(self, part):
        return self.contents[part]
